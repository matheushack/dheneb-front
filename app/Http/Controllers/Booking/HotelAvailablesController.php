<?php

namespace Dheneb\Http\Controllers\Booking;

use Dheneb\Http\Controllers\Controller;
use Dheneb\Models\BookingHotel;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelAvailableRequest;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelBookRequest;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelDetailRequest;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelRoomAvailableRequest;
use Dheneb\Services\Channels\Entities\Requests\Hotel\Tags\PassengerItem;
use Dheneb\Services\Channels\ProviderHotelService;
use Illuminate\Http\Request;
use Dheneb\Services\UserService;
use Carbon\Carbon;

class HotelAvailablesController extends Controller
{

    private $providerHotelService;

    public function __construct(Request $request, ProviderHotelService $providerHotelService)
    {
        $this->middleware('auth');
        $this->providerHotelService = $providerHotelService;
    }

    public function index()
    {
        return view('hotel.hotel-availables');
    }

    public function showSearch(Request $request, BookingHotelAvailableRequest $bookingHotelAvailableRequest)
    {
//        $this->validate($request, [
//            'destination_id' => 'required',
//            'checkin' => 'required|date',
//            'nights' => 'required|integer',
//            'rooms' => 'required|array',
//        ]);

        $bookingHotelAvailableRequest
            ->setDestinationId('Y2l0eS0yMDY=')
//            ->setDestinationId($request->input('destination_id'))
            ->setCheckin(Carbon::parse($request->input('checkin'))->format('Y-m-d'))
            ->setNights($request->input('nights'));

        foreach($request->input('listrooms') as $room){
            $bookingHotelRoomAvailableRequest = new BookingHotelRoomAvailableRequest();
            if (!empty($room['adults']))
                $bookingHotelRoomAvailableRequest->setAdults($room['adults']);
            if (!empty($room['children']))
                $bookingHotelRoomAvailableRequest->setChild($room['children']);

            $bookingHotelAvailableRequest->setRooms($bookingHotelRoomAvailableRequest);
            unset($bookingHotelRoomAvailableRequest);
        }

        $availables = $this->providerHotelService->availables($bookingHotelAvailableRequest);

        return view('hotel.hotel-availables-show', compact('availables'));
    }

    public function detail($hotelId, $roomId, BookingHotelDetailRequest $bookingHotelDetailRequest)
    {
        $bookingHotelDetailRequest
            ->setId($hotelId)
            ->setRoomId($roomId);

        $detail = $this->providerHotelService->detail($bookingHotelDetailRequest);
        $user = \Auth::getUser();

        return view('hotel.hotel-availables-detail', compact('detail', 'user'));
    }

    public function book(Request $request, BookingHotelBookRequest $bookingHotelBookRequest)
    {
//        $this->validate($request, [
//            'id' => 'required',
//            'passengers' => 'required|array'
//        ]);

        $bookingHotelBookRequest
            ->setId($request->input('id'))
            ->setName($request->input('name'))
            ->setLastName($request->input('lastName'))
            ->setEmail($request->input('email'))
            ->setPhone($request->input('phone'));


        foreach($request->input('passengers') as $rooms) {
            foreach ($rooms as $id => $passengers) {
                foreach ($passengers as $passenger) {
                    $passengerItem =  (new PassengerItem)
                        ->setName(data_get($passenger, 'name'))
                        ->setLastName(data_get($passenger, 'lastName'))
                        ->setType(data_get($passenger, 'type'));
                    $bookingHotelBookRequest->setPassengers($passengerItem);
                }

            }
        }

        $book = $this->providerHotelService->book($bookingHotelBookRequest);

        if ($book instanceof BookingHotel) {
            return redirect('/bookings/hotel/' . $book->id);
        }

        dd('tratar erro');
    }

}
