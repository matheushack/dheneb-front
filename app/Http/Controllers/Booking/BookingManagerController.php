<?php

namespace Dheneb\Http\Controllers\Booking;

use Dheneb\Contracts\BookingContracts;
use Dheneb\Http\Controllers\Controller;
use Dheneb\Services\BookingService;
use Dheneb\Services\LineBusinessService;

class BookingManagerController extends Controller
{

	private $bookingService;

    /**
     * @var LineBusinessService
     */
    private $lineBusinessService;

    public function __construct(BookingService $bookingService, LineBusinessService $lineBusinessService)
	{
		$this->bookingService = $bookingService;
        $this->lineBusinessService = $lineBusinessService;
        $this->middleware('auth');
    }

	/**
	 * Show the application dashboard.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('booking.manage', [
			'bookings' => $this->bookingService->getAllDetail()->get()
		]);
	}

	public function detail($product, $id)
	{
        if ($this->lineBusinessService->checkProduct($product)) {
            switch (strtolower($product)) {
                case 'hotel':
                    $booking = $this->bookingService->findHotel($id);
                break;
                case 'tour':
                    $booking = $this->bookingService->findTour($id);
                break;
            }
            if ($booking instanceof BookingContracts) {
                return view('booking.detail_' . $product, [
                    'booking' => $booking
                ]);
            }
        }

        return redirect('/bookings');
	}

}
