<?php
namespace Dheneb\Services\Channels;

use Dheneb\Models\BookingHotel;
use Dheneb\Services\Channels\ChannelService;
use Dheneb\Services\Channels\Contracts\{
    Provider,
    Hotel\Hotel
};
use Dheneb\Services\Channels\Entities\Requests\Hotel\{
    BookingHotelAvailableRequest,
    BookingHotelDetailRequest
};
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelBookRequest;
use Dheneb\Services\Channels\Entities\Response\Hotel\BookingBookResponse;
use Dheneb\Services\Channels\Entities\Response\Hotel\BookingDetailResponse;
use Dheneb\Services\Channels\Entities\Response\Hotel\{
    BookingHotelAvailableResponse
};
use Dheneb\Services\Channels\Services\BookingHotelService;
use Illuminate\Support\Collection;
use Dheneb\Models\ProviderChannel;

class ProviderHotelService
{

    /**
     * @var \Dheneb\Services\Channels\ChannelService
     */
    private $channelService;

    /**
     * ProviderHotelService constructor.
     * @param \Dheneb\Services\Channels\ChannelService $channelService
     */
    public function __construct(ChannelService $channelService)
    {
        $this->channelService = $channelService;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function channels()
    {
        return $this->channelService->listHotelProviders();
    }

    /**
     * @param BookingHotelAvailableRequest $bookingHotelAvailableRequest
     * @return BookingHotelAvailableResponse
     */
    public function availables(BookingHotelAvailableRequest $bookingHotelAvailableRequest)
    {
        $hotelsAvailables = new Collection;
        $this->channels()->each(function($channel) use ($bookingHotelAvailableRequest, &$hotelsAvailables){
            $provider = $this->channelService->callProvider($channel);
            if ($provider instanceof Provider && $provider instanceof Hotel) {
                $hotelsAvailablesResponse = $provider->availables($bookingHotelAvailableRequest);
                if ($hotelsAvailablesResponse instanceof Collection) {
                    $hotelsAvailables = $provider->availables($bookingHotelAvailableRequest)->map(function ($item) use($channel, $bookingHotelAvailableRequest){
                        $item->setId(AvailableRule::idUnionProvider($item->getId(), $channel, $bookingHotelAvailableRequest));
                        return $item;
                    });
                }
            }
        });
        $bookingHotelAvailableResponse = new BookingHotelAvailableResponse;
        $bookingHotelAvailableResponse->setData($hotelsAvailables);
        return $bookingHotelAvailableResponse;
    }

    /**
     * @param BookingHotelDetailRequest $bookingHotelDetailRequest
     * @return bool
     */
    public function detail(BookingHotelDetailRequest $bookingHotelDetailRequest)
    {
        if ($bookingHotelDetailRequest->getProvider() instanceof ProviderChannel) {
            $provider = $this->channelService->callProvider($bookingHotelDetailRequest->getProvider());
            if ($provider instanceof Provider && $provider instanceof Hotel) {
                try {
                    $detail = $provider->detail($bookingHotelDetailRequest);
                    if ($detail instanceof BookingDetailResponse) {
                        $detail->setId(AvailableRule::idUnionProvider($detail->getId(), $bookingHotelDetailRequest->getProvider(), $bookingHotelDetailRequest->getBookingHotelAvailableRequest()));
                        return $detail;
                    }
                } catch (\Exception $e) {
                    dd('ale', $e);
                }
            }
        }
        return false;
    }

    /**
     * @param BookingHotelBookRequest $bookingHotelBookRequest
     */
    public function book(BookingHotelBookRequest $bookingHotelBookRequest)
    {
        $bookingHotelService = new BookingHotelService;
        $bookingHotel = $bookingHotelService->init($bookingHotelBookRequest, \Auth::User());

        if ($bookingHotelBookRequest->getProvider() instanceof ProviderChannel && $bookingHotel instanceof BookingHotel) {
            $provider = $this->channelService->callProvider($bookingHotelBookRequest->getProvider());
            if ($provider instanceof Provider && $provider instanceof Hotel) {
                try {
                    $bookingBookResponse = $provider->book($bookingHotelBookRequest);
                    if ($bookingBookResponse instanceof BookingBookResponse) {
                        $bookingBookResponse = $bookingBookResponse->toModelUpdate($bookingHotel);
                        $bookingBookResponse->save();
                        return $bookingBookResponse;
                    }
                    return false;
                } catch (\Exception $e) {
                    dd($e);
                }
            }
        }
        return false;
    }

}
