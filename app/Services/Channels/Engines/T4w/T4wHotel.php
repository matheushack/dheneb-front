<?php
namespace Dheneb\Services\Channels\Engines\T4w;

use Dheneb\Services\Channels\Contracts\Hotel\Hotel;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelAvailableRequest;
use Dheneb\Services\Channels\Engines\T4w\Services\SearchAvailabilityService;


/**
 * Class T4wHotel
 * @package Dheneb\Services\Channels\Engines\T4w
 */
abstract class T4wHotel implements Hotel
{

    /**
     * @var array
     */
    public $config = [];

    /**
     * T4wHotel constructor.
     * @param array $config
     */
    function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param BookingHotelAvailableRequest $bookingHotelAvailableRequest
     */
    public function availables(BookingHotelAvailableRequest $bookingHotelAvailableRequest)
    {
        $searchAvailabilityService = new SearchAvailabilityService($bookingHotelAvailableRequest, $this->config);
        return $searchAvailabilityService->availables();
    }

}