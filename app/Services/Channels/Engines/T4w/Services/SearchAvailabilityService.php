<?php

namespace Dheneb\Services\Channels\Engines\T4w\Services;

use Dheneb\Services\Channels\Engines\T4w\Factories\BookingHotelAvailableResponseFactory;
use Dheneb\Services\Channels\Engines\T4w\Soap\Factories\SearchAvailabilityRequestFactory;
use Dheneb\Services\Channels\Engines\T4w\Soap\HotelSoapClient;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelAvailableRequest;
use Exception;
use SoapFault;

class SearchAvailabilityService
{

    /**
     * @var BookingHotelAvailableRequest
     */
    private $bookingHotelAvailableRequest;

    /**
     * @var array
     */
    private $config;

    public function __construct(BookingHotelAvailableRequest $bookingHotelAvailableRequest, array $config)
    {
        $this->bookingHotelAvailableRequest = $bookingHotelAvailableRequest;
        $this->config = $config;
    }

    public function availables()
    {
        return $this->makeResponse();
    }

    private function callSearchAvailability()
    {
        $hotelSoapClient = new HotelSoapClient($this->config);
        try {
            return $hotelSoapClient->searchAvailability(
                (new SearchAvailabilityRequestFactory($this->bookingHotelAvailableRequest))->make()
            );
        } catch(SoapFault $SoapFault) {
            throw new Exception($SoapFault->getMessage());
        }
    }

    private function makeResponse()
    {
        return (new BookingHotelAvailableResponseFactory($this->callSearchAvailability(), $this->config))->make();
    }


}