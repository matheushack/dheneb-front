<?php

namespace Dheneb\Services\Channels\Engines\T4w\Factories;

use Dheneb\Services\Channels\Entities\Response\Hotel\BookingHotelAvailableItemResponse;
use Dheneb\Services\Channels\Entities\Response\Hotel\BookingHotelAvailableResponse;
use Dheneb\Services\Channels\Entities\Response\Hotel\BookingRoomAvailableItemResponse;
use Illuminate\Support\Collection;

class BookingHotelAvailableResponseFactory
{

    private $searchAvailabilityResult;

    /**
     * @var array
     */
    private $tags;

    public function __construct($searchAvailabilityResult, array $config = [])
    {
        $this->searchAvailabilityResult = $searchAvailabilityResult;
        $this->tags = $config['soap']['tags'];
    }

    public function make()
    {
        $hotelResponseArray = data_get($this->searchAvailabilityResult, "searchAvailabilityResult.Hotels.{$this->tags['hotelResult']}", []);
        $responseToken = data_get($this->searchAvailabilityResult, 'searchAvailabilityResult.Token');

        $results = new Collection;
        if (count($hotelResponseArray) >= 1){

            foreach($hotelResponseArray as $hotelResponse) {
                $hotelResult = $this->makeHotelResult($hotelResponse, $responseToken);
                if($hotelResult) {
                    $results[] = $hotelResult;
                }
            }
        }
        return $results;
    }

    private function makeHotelResult($hotelResponse, $responseToken)
    {
        $bookingHotelAvailableItemResponse = (new BookingHotelAvailableItemResponse)
            ->setHotelName($hotelResponse->HotelInfo->HotelName)
            ->setPhone(@$hotelResponse->HotelInfo->Phone)
            ->setAddress(@$hotelResponse->HotelInfo->Address)
            ->setHotelCategory((int)@$hotelResponse->HotelInfo->HotelCategory)
            ->setURLThumb(@$hotelResponse->HotelInfo->URLThumb)//verificar caso nao tenha imgagem ir sem imagem
            ->setAmenities(@$hotelResponse->HotelInfo->Amenities)
            ->setCategoryDescription(@$hotelResponse->HotelInfo->CategoryDescription)
            ->setHotelId($hotelResponse->HotelId)
            ->setRecommended($hotelResponse->Recommended)
            ->setNewGroups($hotelResponse->NewGroups)
            ->setPromotions((array)$hotelResponse->Promotions)
            ->setLocationNames($hotelResponse->LocationNames);

        $roomResultCollection = $this->makeRoomResultCollection($hotelResponse, $responseToken);
        $bookingHotelAvailableItemResponse->setRooms($roomResultCollection->toArray());
        $firstRoom = $roomResultCollection->first();
        if ($firstRoom) {
            $bookingHotelAvailableItemResponse
                ->setCurrency(@$firstRoom->getCurrency())
                ->setLowestPrice(@$firstRoom->getValue());

        }

        return $bookingHotelAvailableItemResponse;
    }

    private function makeRoomResultCollection($hotelResponse, $responseToken)
    {
        $roomResultTag = $this->tags['roomResult'];
        $roomResponseArray = $hotelResponse->Rooms->$roomResultTag;
        $results = new Collection;
        foreach($roomResponseArray as $roomResponse) {
            $roomResult = $this->makeRoomResult($roomResponse, $responseToken);
            if($roomResult) {
                $results[] = $roomResult;
            }
        }
        return $results;
    }

    /**
     * @param $roomResponse
     * @param $responseToken
     * @return BookingRoomAvailableItemResponse|null
     */
    public function makeRoomResult($roomResponse, $responseToken)
    {
        if(!data_get($roomResponse, 'RoomDescription')) {
            return null;
        }
        return (new BookingRoomAvailableItemResponse)
            ->setId(zipString($roomResponse->RoomId . $responseToken))
            ->setQtyAdults($roomResponse->QtyAdults)
            ->setAgeOfTheChildren($roomResponse->AgeOfTheChildren)
            ->setSellPricePerRoomCurrency($roomResponse->SellPricePerRoom->Currency)
            ->setSellPricePerRoomValue(money($roomResponse->SellPricePerRoom->Value, $roomResponse->SellPricePerRoom->Currency))
            ->setCurrency($roomResponse->SellPriceTotal->Currency)
            ->setValue(money($roomResponse->SellPriceTotal->Value, $roomResponse->SellPriceTotal->Currency))
            ->setBoardDescription(data_get($roomResponse, 'BoardDescription'))
            ->setRoomDescription(data_get($roomResponse, 'RoomDescription'))
            ->setAvailable((bool)$roomResponse->Available)
            ->setHotelCategory((int)@$roomResponse->HotelCategory)
            ->setSpecialOffer((bool)$roomResponse->SpecialOffer)
            ->setQuantity($roomResponse->Quantity)
            ->setBreakdownSales((array)$roomResponse->BreakdownSales)
            ->setAmenities($roomResponse->Amenities)
            ->setRoomPictures((array)$roomResponse->RoomPictures)
            ->setTaxesIncluded((bool)$roomResponse->TaxesIncluded)
            ->setRemarks($roomResponse->Remarks)
            ->setTermsAndConditions($roomResponse->TermsAndConditions)
            ->setRecommendedRoom((bool)$roomResponse->RecommendedRoom)
            ->setDynamicInventory((bool)$roomResponse->DynamicInventory)
            ->setHotelDirectPayment((bool)$roomResponse->HotelDirectPayment)
            ->setBreakfastIncluded((bool)$roomResponse->BreakfastIncluded)
            ->setIsNonRefundable((bool)$roomResponse->IsNonRefundable);
    }



}