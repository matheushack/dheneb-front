<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap;


use Dheneb\Services\Channels\Engines\T4w\Soap\Requests\SearchAvailabilityRequest;

class HotelSoapClient extends CangoorooSoapClient
{

    public function __construct(array $config)
    {
        parent::__construct($config['base_url'] . '/' . $config['soap']['wsdl']['hotel'], $config);
    }


    public function searchAvailability(SearchAvailabilityRequest $searchAvailabilityRequest)
    {
        $request = $this->getCredential();
        $request->criteria = $searchAvailabilityRequest->criteria;
        return parent::searchAvailability($request);
    }

}