<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap;

use Dheneb\Services\Channels\Engines\T4w\Soap\Requests\Tags\Credential;
use Dheneb\Services\Clients\SoapService;
use Dheneb\Services\Channels\Engines\T4w\Soap\Requests\Request;

abstract class CangoorooSoapClient extends SoapService
{

    const TIMEOUT = 200;

    public $config = [];

    public function __construct($wsdl = null, array $config = [])
    {
        $this->config = $config;
        parent::__construct($wsdl, $this->getSoapOptions($wsdl, []));
    }

    /**
     * @param $wsdlUrl
     * @param array $options
     * @return array
     */
    private function getSoapOptions($wsdlUrl, array $options)
    {
        $defaultOptions = [
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
            'cache_wsdl' => WSDL_CACHE_DISK,
            'trace' => 1,
            'exceptions' => true,
            'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
            'connection_timeout' => self::TIMEOUT,
            'location' => str_replace('?WSDL', '', $wsdlUrl),
            'stream_context' => $this->getStreamContext(),
        ];
        return $options + $defaultOptions;
    }

    /**
     * @return resource
     */
    private function getStreamContext()
    {
        return  stream_context_create(array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'ssl_method' => SOAP_SSL_METHOD_SSLv3,
                'connection_timeout' => self::TIMEOUT
            )
        ));
    }

    public function __doRequest($request, $location, $action, $version, $one_way = null)
    {
        $action_check = explode('/', $action);
        if (in_array('getBookingList', $action_check)) {
            $request = preg_replace('/\<ns2:[a-zA-Z]+\/\>/', '', $request);
        }

        return parent::__doRequest($request, $location, $action, $version, $one_way);
    }

    public function getCredential()
    {
        $credential = new Credential;
        $credential->UserName = $this->config['username'];
        $credential->Password = $this->config['password'];

        $request = new Request;
        $request->credential = $credential;

        return $request;
    }

}