<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Factories;

use Dheneb\Services\Channels\Engines\T4w\Soap\Requests\SearchAvailabilityRequest;
use Dheneb\Services\Channels\Engines\T4w\Soap\Requests\Tags\SearchAvailability\Criteria;
use Dheneb\Services\Channels\Engines\T4w\Soap\Requests\Tags\SearchAvailability\ResultManipulation;
use Dheneb\Services\Channels\Engines\T4w\Soap\Requests\Tags\SearchAvailability\ResultPagination;
use Dheneb\Services\Channels\Engines\T4w\Soap\Requests\Tags\SearchAvailability\Room;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelAvailableRequest;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelRoomAvailableRequest;

class SearchAvailabilityRequestFactory
{

    /**
     * @var BookingHotelAvailableRequest
     */
    private $bookingHotelAvailableRequest;

    public function __construct(BookingHotelAvailableRequest $bookingHotelAvailableRequest)
    {
        $this->bookingHotelAvailableRequest = $bookingHotelAvailableRequest;
    }

    public function make()
    {
        $request = new SearchAvailabilityRequest();
        $request->criteria = $this->makeCriteria();
        return $request;
    }


    public function makeCriteria()
    {
        $criteria = new Criteria;
        $criteria->CityId = $this->bookingHotelAvailableRequest->getDestinationId();
        $criteria->CheckInDate = $this->bookingHotelAvailableRequest->getCheckin();
        $criteria->NumberOfNights = $this->bookingHotelAvailableRequest->getNights();
        $criteria->AccomodationSearchType = 'Hotel';
        $criteria->ReturnRoomOnRequest = true;
        $criteria->Rooms = $this->makeRooms();
        $criteria->ResultManipulation = $this->makeResultManipulation();
        $criteria->ResultPagination = $this->makeResultPagination();
        return $criteria;
    }

    public function makeRooms()
    {
        $rooms = array();
        foreach($this->bookingHotelAvailableRequest->getRooms() as $bookingHotelRoomAvailableRequest) {
            $rooms[] = $this->makeRoom($bookingHotelRoomAvailableRequest);
        }
        return $rooms;
    }

    public function makeRoom(BookingHotelRoomAvailableRequest $bookingHotelRoomAvailableRequest)
    {
        $room = new Room;
        $room->Quantity = 1;
        $room->QtyAdults = $bookingHotelRoomAvailableRequest->getAdults();
        $room->AgeOfTheChildrens = $bookingHotelRoomAvailableRequest->getChild();
        return $room;
    }

    public function makeResultManipulation()
    {
        $resultManipulation = new ResultManipulation;
        $resultManipulation->NumberOfPics = 0;
        $resultManipulation->CheapestRoomOnly = false;
        $resultManipulation->OnlyRecommended = false;
        $resultManipulation->MinPrice = 0;
        $resultManipulation->MaxPrice = 0;
        $resultManipulation->DescriptionLanguageISO = 'pt-BR';
        $resultManipulation->ReturnHotelInfo = true;
        $resultManipulation->ReturnFilterInformation = false;
        return $resultManipulation;
    }

    public function makeResultPagination()
    {
        $resultPagination = new ResultPagination;
        $resultPagination->SortDirection = 'ASC';
        $resultPagination->OrderByField = 'Price';
        $resultPagination->From = 0;
        $resultPagination->Range = 50000;
        return $resultPagination;
    }

}