<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests;

class CancelBookingRequest extends Request
{
    public $serviceID;
}
