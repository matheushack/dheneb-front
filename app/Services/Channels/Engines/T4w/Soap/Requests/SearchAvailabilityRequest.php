<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests;

class SearchAvailabilityRequest extends Request
{
    /**
     * @var Tags\SearchAvailability\Criteria
     */
    public $criteria;
}
