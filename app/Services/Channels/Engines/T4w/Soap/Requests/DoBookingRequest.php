<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests;

class DoBookingRequest extends Request
{
    public $Token;

    /**
     * @var Tags\DoBooking\RoomBoking[]
     */
    public $Rooms = array();

    public $BookingId = 0;

    public $ClientReference;

    public $ClientObservation;

    public $ResponsibleOperator = 0;
}
