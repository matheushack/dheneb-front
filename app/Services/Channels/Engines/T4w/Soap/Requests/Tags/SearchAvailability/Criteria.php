<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests\Tags\SearchAvailability;

class Criteria
{
    public $CityId;

    public $CheckInDate;

    public $NumberOfNights;

    public $AccomodationSearchType;

    public $ReturnRoomOnRequest;

    /**
     * @var ResultManipulation
     */
    public $ResultManipulation;

    /**
     * @var Room[]
     */
    public $Rooms = array();

    /**
     * @var ResultPagination
     */
    public $ResultPagination;
}