<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests\Tags;

class Credential
{
    public $UserName;

    public $Password;
}