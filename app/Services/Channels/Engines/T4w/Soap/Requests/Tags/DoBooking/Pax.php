<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests\Tags\DoBooking;

class Pax
{
    public $AddressNumber = 0;

    public $DDI = 0;

    public $DDD = 0;

    public $PhoneNumber = 0;

    public $Birthday = '0001-01-01T00:00:00';

    public $Name;

    public $LastName;

    public $Age = 0;

    public $CPF;

    public $EnumTitle;

    public $EnumPaxType;

    public $PassengerEmail;
}
