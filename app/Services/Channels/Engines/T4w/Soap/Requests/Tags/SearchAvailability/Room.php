<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests\Tags\SearchAvailability;

class Room
{
    public $Quantity;

    public $QtyAdults;

    public $AgeOfTheChildrens;
}