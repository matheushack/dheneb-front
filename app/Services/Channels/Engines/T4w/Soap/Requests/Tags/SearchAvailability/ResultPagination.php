<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests\Tags\SearchAvailability;

class ResultPagination
{
    public $SortDirection;

    public $OrderByField;

    public $From;

    public $Range;
}