<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests\Tags\DoBooking;

class RoomBooking
{
    public $HotelId;

    public $RoomId;

    /**
     * @var Pax[]
     */
    public $Paxs = array();
}
