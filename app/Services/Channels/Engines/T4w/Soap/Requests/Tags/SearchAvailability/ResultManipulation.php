<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests\Tags\SearchAvailability;

class ResultManipulation
{
    public $NumberOfPics;

    public $CheapestRoomOnly;

    public $OnlyRecommended;

    public $MinPrice;

    public $MaxPrice;

    public $DescriptionLanguageISO;

    public $ReturnHotelInfo;

    public $ReturnFilterInformation;
}