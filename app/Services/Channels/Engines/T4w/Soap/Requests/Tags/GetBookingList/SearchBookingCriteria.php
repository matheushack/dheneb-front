<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests\Tags\GetBookingList;

class SearchBookingCriteria
{
    public $BookingNumber = [];

    public $ServiceTypes = [];

    public $PassengerName;

    public $InitialBookingDate;

    public $FinalBookingDate;

    public $InitialServiceDate;

    public $FinalServiceDate;

    public $BookingStatus = [];

    public $PaymentStatus;

    public $CreationUserId;

    public $CityId;

    public $ExternalReference;

    public $PaymentDeadlineStart;

    public $PaymentDeadlineEnd;

    public $CancelationDateStart;

    public $CancelationDateEnd;

    public $LinkedInvoice;

    public $DatePaymentStart;

    public $DatePaymentEnd;

    public $InitialUpdateDate;

    public $FinalUpdateDate;

}
