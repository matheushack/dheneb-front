<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests;

class GetCityDataRequest extends Request
{
    /**
     * @var string
     */
    public $countryId;
}
