<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests;

class Request
{
    /**
     * @var Tags\Credential
     */
    public $credential;
}
