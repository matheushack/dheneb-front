<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests;

class CancelFullBookingRequest extends Request
{
    /**
     * @var int
     */
    public $bookingId;
}
