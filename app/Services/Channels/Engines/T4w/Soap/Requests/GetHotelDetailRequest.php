<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests;

class GetHotelDetailRequest extends Request
{
    /**
     * @var int[]
     */
    public $hotelID = array();

    public $descriptionLangISO;
}
