<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests;

class GetBookingDetailRequest extends Request
{
    /**
     * @var int
     */
    public $bookingId;
}
