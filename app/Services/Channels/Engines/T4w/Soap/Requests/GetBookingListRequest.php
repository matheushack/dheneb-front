<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests;

class GetBookingListRequest extends Request
{
    /**
     * @var string
     */
    public $searchBookingCriteria = [];
}
