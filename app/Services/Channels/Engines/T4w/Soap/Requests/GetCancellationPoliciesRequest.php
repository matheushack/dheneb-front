<?php

namespace Dheneb\Services\Channels\Engines\T4w\Soap\Requests;

class GetCancellationPoliciesRequest extends Request
{
    public $Token;

    public $HotelId;

    public $RoomsIds = array();
}
