<?php

namespace Dheneb\Services\Channels\Engines\Ehtl\Services;

use Dheneb\Services\Channels\Engines\Ehtl\Client\EhtlClient;
use Dheneb\Services\Channels\Engines\Ehtl\Factories\BookingHotelDetailResponseFactory;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelDetailRequest;

/**
 * Class DetailService
 * @package Dheneb\Services\Channels\Engines\Ehtl\Services
 */
class DetailService
{
    /**
     * @var BookingHotelAvailableRequest
     */
    private $bookingHotelDetailRequest;

    /**
     * @var array
     */
    private $config;

    /**
     * @var EhtlClient
     */
    private $ehtlClient;

    /**
     * DetailService constructor.
     * @param BookingHotelDetailRequest $bookingHotelDetailRequest
     * @param array $config
     */
    public function __construct(BookingHotelDetailRequest $bookingHotelDetailRequest, array $config)
    {
        $this->bookingHotelDetailRequest = $bookingHotelDetailRequest;
        $this->config = $config;
        $this->ehtlClient = new EhtlClient($this->config);
    }

    /**
     * @return \Dheneb\Services\Channels\Entities\Response\Hotel\BookingDetailResponse
     */
    public function detail()
    {
         return $this->makeResponse();
    }

    /**
     * @return \Dheneb\Services\Channels\Entities\Response\Hotel\BookingDetailResponse
     */
    private function makeResponse()
    {
        $id = unserialize($this->bookingHotelDetailRequest->getId());
        return (new BookingHotelDetailResponseFactory($this->callSearchAvailability()))->make($id['token']);
    }

    /**
     * @return mixed|string
     */
    private function callSearchAvailability()
    {
        return $this->ehtlClient->detail($this->bookingHotelDetailRequest);
    }

}