<?php

namespace Dheneb\Services\Channels\Engines\Ehtl\Services;

use Dheneb\Services\Channels\Engines\Ehtl\Client\EhtlClient;
use Dheneb\Services\Channels\Engines\Ehtl\Factories\BookingHotelBookResponseFactory;
use Dheneb\Services\Channels\Engines\Ehtl\Factories\BookingHotelDetailResponseFactory;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelBookRequest;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelDetailRequest;

/**
 * Class DetailService
 * @package Dheneb\Services\Channels\Engines\Ehtl\Services
 */
class BookService
{
    /**
     * @var BookingHotelAvailableRequest
     */
    private $bookingHotelDetailRequest;

    /**
     * @var array
     */
    private $config;

    /**
     * @var EhtlClient
     */
    private $ehtlClient;

    /**
     * DetailService constructor.
     * @param BookingHotelDetailRequest $bookingHotelDetailRequest
     * @param array $config
     */
    public function __construct(BookingHotelBookRequest $bookingHotelBookRequest, array $config)
    {
        $this->bookingHotelBookRequest = $bookingHotelBookRequest;
        $this->config = $config;
        $this->ehtlClient = new EhtlClient($this->config);
    }

    /**
     * @return \Dheneb\Services\Channels\Entities\Response\Hotel\BookingDetailResponse
     */
    public function book()
    {
         return $this->makeResponse();
    }

    /**
     * @return \Dheneb\Services\Channels\Entities\Response\Hotel\BookingDetailResponse
     */
    private function makeResponse()
    {
        return (new BookingHotelBookResponseFactory($this->callBook()))->make();
    }

    /**
     * @return mixed|string
     */
    private function callBook()
    {
        return $this->ehtlClient->book($this->bookingHotelBookRequest);
    }

}