<?php

namespace Dheneb\Services\Channels\Engines\Ehtl\Services;

use Dheneb\Services\Channels\Engines\Ehtl\Client\EhtlClient;
use Dheneb\Services\Channels\Engines\Ehtl\Client\Factories\SearchAvailabilityRequestFactory;
use Dheneb\Services\Channels\Engines\Ehtl\Factories\BookingHotelAvailableResponseFactory;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelAvailableRequest;

/**
 * Class SearchAvailabilityService
 * @package Dheneb\Services\Channels\Engines\Ehtl\Services
 */
class SearchAvailabilityService
{

    /**
     * @var BookingHotelAvailableRequest
     */
    private $bookingHotelAvailableRequest;

    /**
     * @var array
     */
    private $config;

    /**
     * @var EhtlClient
     */
    private $ehtlClient;

    /**
     * SearchAvailabilityService constructor.
     * @param BookingHotelAvailableRequest $bookingHotelAvailableRequest
     * @param array $config
     */
    public function __construct(BookingHotelAvailableRequest $bookingHotelAvailableRequest, array $config)
    {
        $this->bookingHotelAvailableRequest = $bookingHotelAvailableRequest;
        $this->config = $config;
        $this->ehtlClient = new EhtlClient($this->config);
    }

    /**
     * @return array|\Illuminate\Support\Collection
     */
    public function availables()
    {
        return $this->makeResponse();
    }

    /**
     * @return array|\Illuminate\Support\Collection
     */
    private function makeResponse()
    {
        return (new BookingHotelAvailableResponseFactory($this->callSearchAvailability(), $this->ehtlClient->getToken()))->make();
    }

    /**
     * @return mixed|string
     */
    private function callSearchAvailability()
    {
        return $this->ehtlClient->hotelsAvailabilities((new SearchAvailabilityRequestFactory($this->bookingHotelAvailableRequest))->make());
    }

}