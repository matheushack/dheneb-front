<?php

namespace Dheneb\Services\Channels\Engines\Ehtl;

use Dheneb\Services\Channels\Contracts\Hotel\Hotel;
use Dheneb\Services\Channels\Engines\Ehtl\Services\BookService;
use Dheneb\Services\Channels\Engines\Ehtl\Services\DetailService;
use Dheneb\Services\Channels\Engines\Ehtl\Services\SearchAvailabilityService;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelAvailableRequest;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelBookRequest;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelDetailRequest;

abstract class EhtlHotel implements Hotel
{

    /**
     * @var array
     */
    public $config = [];

    /**
     * T4wHotel constructor.
     * @param array $config
     */
    function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param BookingHotelAvailableRequest $bookingHotelAvailableRequest
     */
    public function availables(BookingHotelAvailableRequest $bookingHotelAvailableRequest)
    {
        $searchAvailabilityService = new SearchAvailabilityService($bookingHotelAvailableRequest, $this->config);
        return $searchAvailabilityService->availables();
    }

    public function detail(BookingHotelDetailRequest $bookingHotelDetailRequest)
    {
        $detailService = new DetailService($bookingHotelDetailRequest, $this->config);
        return $detailService->detail();
    }

    public function book(BookingHotelBookRequest $bookingHotelBookRequest)
    {
        $detailService = new BookService($bookingHotelBookRequest, $this->config);
        return $detailService->book();
    }

}