<?php

namespace Dheneb\Services\Channels\Engines\Ehtl\Factories;

use Dheneb\Services\Channels\Entities\Response\Hotel\BookingHotelAvailableItemResponse;
use Dheneb\Services\Channels\Entities\Response\Hotel\BookingHotelAvailableResponse;
use Dheneb\Services\Channels\Entities\Response\Hotel\BookingRoomAvailableItemResponse;
use Dheneb\Services\Channels\Entities\Response\Hotel\BookingRoomAvailableResponse;
use Illuminate\Support\Collection;

/**
 * Class BookingHotelAvailableResponseFactory
 * @package Dheneb\Services\Channels\Engines\Ehtl\Factories
 */
class BookingHotelAvailableResponseFactory
{

    /**
     * @var
     */
    private $searchAvailabilityResult;

    /**
     * @var array
     */
    private $token;

    /**
     * BookingHotelAvailableResponseFactory constructor.
     * @param $searchAvailabilityResult
     * @param null $token
     */
    public function __construct($searchAvailabilityResult, $token = null)
    {
        $this->searchAvailabilityResult = $searchAvailabilityResult;
        $this->token = $token;
    }

    /**
     * @return array|Collection
     */
    public function make()
    {
        $results = new Collection;
        if (!empty($this->searchAvailabilityResult->data)) {
            foreach ($this->searchAvailabilityResult->data as $hotelsAvailability) {
                $hotelResult = $this->makeHotelResult($hotelsAvailability);
                if($hotelResult) {
                    $results[] = $hotelResult;
                }
            }
        }
        return $results;
    }

    /**
     * @param $hotelResponse
     * @return BookingHotelAvailableItemResponse
     */
    private function makeHotelResult($hotelResponse)
    {
        $uRLThumb = '';
        if (count($hotelResponse->attributes->hotelImages)) {
            $uRLThumb = $hotelResponse->attributes->hotelImages[rand(0, count($hotelResponse->attributes->hotelImages) - 1 )];
        }

        $bookingHotelAvailableItemResponse = (new BookingHotelAvailableItemResponse)
            ->setId(serialize(['id' => $hotelResponse->id, 'token' => $this->token]))
            ->setHotelName($hotelResponse->attributes->hotel)
            ->setDescription($hotelResponse->attributes->hotelDescription)
            ->setAddress($hotelResponse->attributes->hotelAddress)
            ->setNeighborhood($hotelResponse->attributes->hotelNeighborhood)
            ->setPostalCode($hotelResponse->attributes->hotelPostalCode)
            ->setCategory($hotelResponse->attributes->hotelCategory)
            ->setStars((int)$hotelResponse->attributes->hotelStars)
            ->setLongitude($hotelResponse->attributes->hotelLongitude)
            ->setLatitude($hotelResponse->attributes->hotelLatitude)
            ->setImagens($hotelResponse->attributes->hotelImages)
            ->setCurrency($hotelResponse->attributes->priceCurrency)
            ->setLowerPrice($hotelResponse->attributes->hotelLowerPrice)
            ->setURLThumb($uRLThumb)
            ->setRooms($this->makeRoomResultCollection($hotelResponse->attributes->hotelRooms));

        return $bookingHotelAvailableItemResponse;
    }

    /**
     * @param $rooms
     * @return array|Collection
     */
    private function makeRoomResultCollection($rooms)
    {
        $results = new Collection;
        foreach($rooms as $roomResponse) {

            $roomResult = $this->makeRoomResult($roomResponse, new BookingRoomAvailableResponse);
            if($roomResult) {
                $results[] = $roomResult;
            }
        }
        return $results;
    }


    /**
     * @param $room
     * @param BookingRoomAvailableResponse $bookingRoomAvailableResponse
     * @return BookingRoomAvailableResponse
     */
    private function makeRoomResult($room, BookingRoomAvailableResponse $bookingRoomAvailableResponse)
    {
        foreach ($room->roomsDetail as $roomDetail) {
            $bookingRoomAvailableItemResponse = new BookingRoomAvailableItemResponse;
            $bookingRoomAvailableItemResponse
                ->setCount($roomDetail->count)
                ->setRoomName($roomDetail->roomName)
                ->setType($roomDetail->type)
                ->setRegime($roomDetail->regime)
                ->setAdults($roomDetail->adults)
                ->setChildren($roomDetail->children)
                ->setCurrency($roomDetail->currency)
                ->setPrice($roomDetail->price)
                ->setPriceWithTax($roomDetail->priceWithTax)
                ->setOffer($roomDetail->offer)
                ->setRemarks($roomDetail->remarks)
                ->setIrrevocableGuarantee($roomDetail->irrevocableGuarantee);
            $bookingRoomAvailableResponse->setData($bookingRoomAvailableItemResponse);
        }
        $bookingRoomAvailableResponse->setId($room->roomCode);
        $bookingRoomAvailableResponse->setTotalRoomsPrice($room->totalRoomsPrice);
        $bookingRoomAvailableResponse->setTotalRoomsPriceWithTax($room->totalRoomsPriceWithTax);
        $bookingRoomAvailableResponse->setIrrevocableGuarantee($room->irrevocableGuarantee);

        return $bookingRoomAvailableResponse;
    }

}