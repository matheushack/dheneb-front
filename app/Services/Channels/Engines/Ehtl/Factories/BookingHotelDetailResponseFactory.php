<?php

namespace Dheneb\Services\Channels\Engines\Ehtl\Factories;

use Dheneb\Services\Channels\Entities\Response\Hotel\BookingDetailResponse;
use Dheneb\Services\Channels\Entities\Response\Hotel\Tags\PassengerItem;
use Dheneb\Services\Channels\Entities\Response\Hotel\Tags\RoomItem;

class BookingHotelDetailResponseFactory
{

    private $bookingHotelDetail;

    private $bookingDetailResponse;

    public function __construct($bookingHotelDetail)
    {
        $this->bookingHotelDetail = $bookingHotelDetail->data;
        $this->bookingDetailResponse = new BookingDetailResponse;
    }

    public function make($token = null)
    {
        $this->bookingDetailResponse
            ->setId($token)
            ->setCancellationDeadline($this->bookingHotelDetail->attributes->cancellationDeadline)
            ->setCancellationFee($this->bookingHotelDetail->attributes->cancellationFee)
            ->setIrrevocableGuarantee($this->bookingHotelDetail->attributes->Hotel->irrevocableGuarantee)
            ->setBreakfastIncluded($this->bookingHotelDetail->attributes->Hotel->breakfastIncluded)
            ->setAcceptsDirectPayment($this->bookingHotelDetail->attributes->Hotel->acceptsDirectPayment)
            ->setPoliciesCancellations($this->bookingHotelDetail->attributes->policiesCancellations)
            ->setPoliciesConsiderations($this->bookingHotelDetail->attributes->policiesConsiderations)
            ->setCurrency($this->bookingHotelDetail->attributes->currency)
            ->setTotalPrice($this->bookingHotelDetail->attributes->totalPrice)
            ->setTotalPriceWithTax($this->bookingHotelDetail->attributes->totalPriceWithTax);

        $this->makeRooms();
        return $this->bookingDetailResponse;
    }

    private function makeTaxs()
    {

    }

    private function makeRooms()
    {
        $rooms = $this->bookingHotelDetail->attributes->Rooms;
        foreach ($rooms as $room){
            $roomItem = new RoomItem();
            $roomItem->setCount($room->count);
            $roomItem->setRoomName($room->roomName);
            $roomItem->setType($room->type);
            $roomItem->setAdults($room->adults);
            $roomItem->setChildren($room->children);
            $roomItem->setCurrency($room->currency);
            $roomItem->setPrice($room->price);
            $roomItem->setPriceWithTax($room->priceWithTax);
            $this->bookingDetailResponse->setRooms($roomItem);
        }
    }

}