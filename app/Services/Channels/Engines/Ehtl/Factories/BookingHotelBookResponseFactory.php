<?php

namespace Dheneb\Services\Channels\Engines\Ehtl\Factories;

use Dheneb\Services\Channels\Entities\Response\Hotel\BookingBookResponse;

/**
 * Class BookingHotelBookResponseFactory
 * @package Dheneb\Services\Channels\Engines\Ehtl\Factories
 */
class BookingHotelBookResponseFactory
{

    /**
     * @var
     */
    private $bookingHotelBook;

    /**
     * @var BookingBookResponse
     */
    private $bookingBookResponse;

    /**
     * BookingHotelBookResponseFactory constructor.
     * @param $bookingHotelBook
     */
    public function __construct($bookingHotelBook)
    {
        $this->bookingHotelBook = $bookingHotelBook->data;
        $this->bookingBookResponse = new BookingBookResponse;
    }

    /**
     * @return BookingBookResponse
     */
    public function make()
    {
        $this->bookingBookResponse
            ->setId(data_get($this->bookingHotelBook, 'id'))
            ->setHotelId(1)
            ->setAdults(data_get($this->bookingHotelBook, 'attributes.adults'))
            ->setCheckin(data_get($this->bookingHotelBook, 'attributes.checkin'))
            ->setCheckout(data_get($this->bookingHotelBook, 'attributes.checkout'))
            ->setBookingStatusId(1)
            ->setValue(data_get($this->bookingHotelBook, 'attributes.value'))
            ->setValueWithTax(data_get($this->bookingHotelBook, 'attributes.value_with_tax'))
            ->setCancellationFee(data_get($this->bookingHotelBook, 'attributes.cancellationFee'))
            ->setIrrevocableGuarantee(data_get($this->bookingHotelBook, 'attributes.irrevocableGuarantee'));

        return $this->bookingBookResponse;
    }

    private function makeRoom()
    {

    }

}