<?php

namespace Dheneb\Services\Channels\Engines\Ehtl\Client\Entities\Requests;

class HotelsAvailabilitiesRequest
{

    private $destination_id;

    private $checkin;

    private $nights;

    private $rooms = [];

    /**
     * Get the value of Destination Id
     *
     * @return mixed
     */
    public function getDestinationId()
    {
        return $this->destination_id;
    }

    /**
     * Set the value of Destination Id
     *
     * @param mixed destination_id
     *
     * @return self
     */
    public function setDestinationId($destination_id)
    {
        $this->destination_id = $destination_id;

        return $this;
    }

    /**
     * Get the value of Checkin
     *
     * @return mixed
     */
    public function getCheckin()
    {
        return $this->checkin;
    }

    /**
     * Set the value of Checkin
     *
     * @param mixed checkin
     *
     * @return self
     */
    public function setCheckin($checkin)
    {
        $this->checkin = $checkin;

        return $this;
    }

    /**
     * Get the value of Nights
     *
     * @return mixed
     */
    public function getNights()
    {
        return $this->nights;
    }

    /**
     * Set the value of Nights
     *
     * @param mixed nights
     *
     * @return self
     */
    public function setNights($nights)
    {
        $this->nights = $nights;

        return $this;
    }

    /**
     * Get the value of Rooms
     *
     * @return mixed
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Set the value of Rooms
     *
     * @param mixed rooms
     *
     * @return self
     */
    public function setRooms(HotelsAvailabilitiesRoomRequest $rooms)
    {
        $this->rooms[] = $rooms;

        return $this;
    }

}
