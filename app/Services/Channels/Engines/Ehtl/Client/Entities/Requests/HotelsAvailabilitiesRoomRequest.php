<?php

namespace Dheneb\Services\Channels\Engines\Ehtl\Client\Entities\Requests;

class HotelsAvailabilitiesRoomRequest
{

    private $adults = 1;

    private $children = 0;

    private $childrenages = [];


    /**
     * Get the value of Adults
     *
     * @return mixed
     */
    public function getAdults()
    {
        return $this->adults;
    }

    /**
     * Set the value of Adults
     *
     * @param mixed adults
     *
     * @return self
     */
    public function setAdults($adults)
    {
        $this->adults = $adults;

        return $this;
    }

    /**
     * Get the value of Children
     *
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set the value of Children
     *
     * @param mixed children
     *
     * @return self
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get the value of Childrenages
     *
     * @return mixed
     */
    public function getChildrenages()
    {
        return $this->childrenages;
    }

    /**
     * Set the value of Childrenages
     *
     * @param mixed childrenages
     *
     * @return self
     */
    public function setChildrenages($childrenages)
    {
        $this->childrenages = $childrenages;

        return $this;
    }

}
