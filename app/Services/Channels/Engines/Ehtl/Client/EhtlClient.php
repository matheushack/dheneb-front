<?php

namespace Dheneb\Services\Channels\Engines\Ehtl\Client;

use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelBookRequest;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelDetailRequest;
use GuzzleHttp\Client;
use Dheneb\Services\Channels\Engines\Ehtl\Client\Entities\Requests\{
    HotelsAvailabilitiesRequest,
    HotelsAvailabilitiesRoomRequest
};

/**
 * Class EhtlClient
 * @package Dheneb\Services\Channels\Engines\Ehtl\Client
 */
class EhtlClient
{

    /**
     * @var Client
     */
    private $client;

    /**
     * @var
     */
    public $header;
    /**
     * @var array
     */
    private $config;

    /**
     * EhtlClient constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;

        $this->client = new Client([
            'base_uri' => $this->config['base_url']
        ]);
    }

    /**
     * @return array|bool
     */
    public function getToken()
    {
        if (!empty($this->header))
            return $this->header;

        $request = $this->client->post('oauth/access_token', [
        	'form_params' => [
        	    'username' => $this->config['username'],
        	    'password' => $this->config['password']
        	]
        ]);

        $oauth = json_decode($request->getBody());
        if (!empty($oauth->access_token)) {
            return $this->header = [
                'Authorization' => 'Bearer ' . $oauth->access_token,
                'Content-Type' => 'application/json'
            ];
        }

        return false;
    }

    public function setToken($token)
    {
        $this->header = $token;
    }

    /**
     * @param HotelsAvailabilitiesRequest $hotelsAvailabilitiesRequest
     * @return mixed|string
     */
    public function hotelsAvailabilities(HotelsAvailabilitiesRequest $hotelsAvailabilitiesRequest)
    {
        $requestRooms = $hotelsAvailabilitiesRequest->getRooms();
        $rooms = [];
        foreach($requestRooms as $requestRoom){
            $rooms[] = [
                'adults' => $requestRoom->getAdults(),
                'children' => $requestRoom->getChildren(),
                'childrenages' => [

                ]
            ];
        }
        $body['data'] = [
            'attributes' => [
                'destinationId' => $hotelsAvailabilitiesRequest->getDestinationId(),
                'checkin' => $hotelsAvailabilitiesRequest->getCheckin(),
                'nights' => $hotelsAvailabilitiesRequest->getNights(),
                'roomsAmount' => count($rooms),
                'rooms' => $rooms,
                'perPage' => 9999,
                'hotel' => ''
            ]
        ];
        try {
            $request = $this->client->post('booking/hotels-availabilities', [
            	'headers' => $this->getToken(),
            	'body' => json_encode($body)
            ]);
            $availabilities = json_decode($request->getBody());
        } catch (Exception $e) {
            $availabilities = '';
        }

        return $availabilities;
    }

    /**
     * @param BookingHotelDetailRequest $bookingHotelDetailRequest
     * @return mixed|string
     */
    public function detail(BookingHotelDetailRequest $bookingHotelDetailRequest)
    {
        $id = unserialize($bookingHotelDetailRequest->getId());
        $this->setToken($id['token']);

        $body['data'] = [
            'attributes' => [
                'hotelCode' => $id['id'],
                'roomCode' => $bookingHotelDetailRequest->getRoomId()
            ]
        ];
        try {
            $request = $this->client->post('booking/detail', [
                'headers' => $this->getToken(),
                'body' => json_encode($body)
            ]);
            $detail = json_decode($request->getBody());
        } catch (Exception $e) {
            $detail = '';
        }

        return $detail;
    }

    public function book(BookingHotelBookRequest $bookingHotelBookRequest)
    {
        $this->setToken($bookingHotelBookRequest->getId());
        $passengers = [];

        foreach($bookingHotelBookRequest->getPassengers() as $passenger){
            $passengers[0]['roomsPassenger'][] = [
                'name' => $passenger->getName(),
                'lastName' => $passenger->getLastName()
            ];
        }

        $body['data'] = [
            'attributes' => [
                'name' => $bookingHotelBookRequest->getName(),
                'lastName' => $bookingHotelBookRequest->getLastName(),
                'email' => $bookingHotelBookRequest->getEmail(),
                'phone' => $bookingHotelBookRequest->getPhone(),
                'passengers' => $passengers,
                'paymentsTypes' => 'credit_card',
                "customerName" => "Teste Fluxo Rede",
                "customerEmail" => "compradot@teste.com",
                "customerIdentity" => "38827243828",
                "numberOfPayments"=> "1",
                "cardFlag" => "997",
                "cardHolder" => "Comprador Teste",
                "cardNumber" => "0000000000000001",
                "cardSecurityCode" => "111",
                "cardExpirationDate" => "07/2017"
            ]
        ];

//        dd($bookingHotelBookRequest);
        try {
            $request = $this->client->post('booking/book-hotel', [
                'headers' => (array)$this->getToken(),
                'body' => json_encode($body)
            ]);
            $book = json_decode($request->getBody());
        } catch (Exception $e) {
            $book = '';
        }
        return $book;
    }

}
