<?php

namespace Dheneb\Services\Channels\Engines\Ehtl\Client\Factories;

use Dheneb\Services\Channels\Engines\Ehtl\Client\Entities\Requests\HotelsAvailabilitiesRequest;
use Dheneb\Services\Channels\Engines\Ehtl\Client\Entities\Requests\HotelsAvailabilitiesRoomRequest;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelAvailableRequest;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelRoomAvailableRequest;

class SearchAvailabilityRequestFactory
{

    /**
     * @var BookingHotelAvailableRequest
     */
    private $bookingHotelAvailableRequest;

    public function __construct(BookingHotelAvailableRequest $bookingHotelAvailableRequest)
    {
        $this->bookingHotelAvailableRequest = $bookingHotelAvailableRequest;
        $this->hotelsAvailabilitiesRequest = new HotelsAvailabilitiesRequest;
    }

    public function make()
    {
//        $this->bookingHotelAvailableRequest->setDestinationId('Y2l0eS0yMDY=');
        $this->hotelsAvailabilitiesRequest
            ->setDestinationId($this->bookingHotelAvailableRequest->getDestinationId())
            ->setCheckin($this->bookingHotelAvailableRequest->getCheckin())
            ->setNights($this->bookingHotelAvailableRequest->getNights());

        $this->makeRooms();

        return $this->hotelsAvailabilitiesRequest;
    }

    private function makeRooms()
    {
        $rooms = $this->bookingHotelAvailableRequest->getRooms();
        foreach($rooms as $room){
            $hotelsAvailabilitiesRoomRequest = new HotelsAvailabilitiesRoomRequest();
            $hotelsAvailabilitiesRoomRequest
                ->setAdults($room->getAdults())
                ->setChildren($room->getChild())
                ->setChildrenages($room->getAge());
            $this->hotelsAvailabilitiesRequest->setRooms($hotelsAvailabilitiesRoomRequest);
            unset($hotelsAvailabilitiesRoomRequest);
        }
    }


}