<?php
namespace Dheneb\Services\Channels\Providers\Hotel\Quasar;

use Dheneb\Services\Channels\Contracts\Hotel\Hotel;
use Dheneb\Services\Channels\Entities\Requests\Hotel\{
    BookingHotelAvailableRequest
};
use Dheneb\Services\Channels\Providers\Hotel\Quasar\Entities\Requests\{
    HotelsAvailabilitiesRequest,
    HotelsAvailabilitiesRoomRequest
};
use Dheneb\Services\Channels\Entities\Response\Hotel\{
    BookingHotelAvailableItemResponse,
    BookingHotelAvailableImagensResponse,
    BookingHotelAvailableRoomsResponse,
    BookingHotelAvailableRoomitemResponse
};
use Dheneb\Services\Channels\Providers\Hotel\Quasar\Client\Connection;
use Illuminate\Support\Collection;

class QuasarHotel__old implements Hotel
{

    private $connection;

    function __construct()
    {
        $this->connection = new Connection();
    }

    public function availables(BookingHotelAvailableRequest $bookingHotelAvailableRequest)
    {
        //setar destino
        $bookingHotelAvailableRequest->setDestinationId('Y2l0eS0yMDY=');

        $hotelsAvailabilitiesRequest = new HotelsAvailabilitiesRequest();
        $hotelsAvailabilitiesRequest
            ->setDestinationId($bookingHotelAvailableRequest->getDestinationId())
            ->setCheckin($bookingHotelAvailableRequest->getCheckin())
            ->setNights($bookingHotelAvailableRequest->getNights());

        $rooms = $bookingHotelAvailableRequest->getRooms();
        foreach($rooms as $room){
            $hotelsAvailabilitiesRoomRequest = new HotelsAvailabilitiesRoomRequest();
            $hotelsAvailabilitiesRoomRequest
                ->setAdults($room->getAdults())
                ->setChildren($room->getChild())
                ->setChildrenages($room->getAge());
            $hotelsAvailabilitiesRequest->setRooms($hotelsAvailabilitiesRoomRequest);
            unset($hotelsAvailabilitiesRoomRequest);
        }
        $hotelsAvailabilities = $this->connection->hotelsAvailabilities($hotelsAvailabilitiesRequest);
        $hotels = new Collection();
        if (!empty($hotelsAvailabilities->data)){
            foreach($hotelsAvailabilities->data as $hotelsAvailability){
                $bookingHotelAvailableResponse = new BookingHotelAvailableItemResponse();
                $bookingHotelAvailableRoomsResponse = new BookingHotelAvailableRoomsResponse();
                $bookingHotelAvailableImagensResponse = new BookingHotelAvailableImagensResponse();
                //imagens
                if (is_array($hotelsAvailability->attributes->hotelImages)) {
                    foreach($hotelsAvailability->attributes->hotelImages as $image){
                        $bookingHotelAvailableImagensResponse->setUrl($image);
                    }
                    $bookingHotelAvailableResponse->setImagens($bookingHotelAvailableImagensResponse);
                }
                //rooms
                if (is_array($hotelsAvailability->attributes->hotelRooms)) {
                    foreach($hotelsAvailability->attributes->hotelRooms as $room){
                        $bookingHotelAvailableRoomitemResponse = new BookingHotelAvailableRoomitemResponse();
                        $bookingHotelAvailableRoomitemResponse
                            ->setId($room->roomCode)
                            ->setIrrevocableGuarantee($room->irrevocableGuarantee)
                            ->setTotal($room->totalRoomsPrice)
                            ->setTotalWithTax($room->totalRoomsPriceWithTax);
                        $bookingHotelAvailableRoomsResponse->setRooms($bookingHotelAvailableRoomitemResponse);
                        unset($bookingHotelAvailableRoomitemResponse);
                    }
                    $bookingHotelAvailableResponse->setImagens($bookingHotelAvailableImagensResponse);
                }
                $bookingHotelAvailableResponse->setRooms($bookingHotelAvailableRoomsResponse);

                // dd($hotelsAvailability->attributes->hotelRooms);

                $bookingHotelAvailableResponse
                    ->setId($hotelsAvailability->id)
                    ->setHotel($hotelsAvailability->attributes->hotel)
                    ->setDescription($hotelsAvailability->attributes->hotelDescription)
                    ->setAddress($hotelsAvailability->attributes->hotelAddress)
                    ->setNeighborhood($hotelsAvailability->attributes->hotelNeighborhood)
                    ->setPostalCode($hotelsAvailability->attributes->hotelPostalCode)
                    ->setCategory($hotelsAvailability->attributes->hotelCategory)
                    ->setStars($hotelsAvailability->attributes->hotelStars)
                    ->setLongitude($hotelsAvailability->attributes->hotelLongitude)
                    ->setLatitude($hotelsAvailability->attributes->hotelLatitude)
                    ->setLowerPrice($hotelsAvailability->attributes->hotelLowerPrice)
                    ->setCurrency($hotelsAvailability->attributes->priceCurrency)
                    ->setIrrevocableGuarantee($hotelsAvailability->attributes->irrevocableGuarantee)
                    ->setBreakfastIncluded($hotelsAvailability->attributes->breakfastIncluded)
                    ->setSignsBill($hotelsAvailability->attributes->signsBill)
                    ->setPrePayment($hotelsAvailability->attributes->prePayment);
                $hotels[] = $bookingHotelAvailableResponse;

                unset($bookingHotelAvailableResponse);
            }
        }
        return $hotels;
    }

}
