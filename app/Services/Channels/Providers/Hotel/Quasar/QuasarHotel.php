<?php
namespace Dheneb\Services\Channels\Providers\Hotel\Quasar;

use Dheneb\Services\Channels\Engines\Ehtl\EhtlHotel;

class QuasarHotel extends EhtlHotel
{

    public function __construct()
    {
        parent::__construct((array)config('providers.quasar'));
    }

}