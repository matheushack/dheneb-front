<?php

namespace Dheneb\Services\Channels\Providers\Hotel\Ezlink;

use Dheneb\Services\Channels\Engines\T4w\T4wHotel;

class EzlinkHotel extends T4wHotel
{

    public function __construct()
    {
        parent::__construct((array)config('providers.ezlink'));
    }

}