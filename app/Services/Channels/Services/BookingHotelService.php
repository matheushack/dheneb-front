<?php

namespace Dheneb\Services\Channels\Services;


use Dheneb\Models\BookingHotel;
use Dheneb\Models\BookingStatus;
use Dheneb\Models\User;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelBookRequest;
use Carbon\Carbon;

class BookingHotelService
{

    public function init(BookingHotelBookRequest $bookingHotelBookRequest, User $user)
    {
        $bookingHotelAvailableRequest = $bookingHotelBookRequest->getBookingHotelAvailableRequest();
        $bookingHotel = new BookingHotel();
        $bookingHotel->user_id = $user->id;
        $bookingHotel->hotel_id = 1;
        $bookingHotel->provider_id = $bookingHotelBookRequest->getProvider()->id;
        $bookingHotel->booking_status_id = BookingStatus::WAITING_PROVIDER;
        $bookingHotel->checkin = $bookingHotelAvailableRequest->getCheckin();
        $bookingHotel->checkout = Carbon::parse($bookingHotelAvailableRequest->getCheckin())->addDay($bookingHotelAvailableRequest->getNights())->format('Y-m-d');
        $bookingHotel->adults = 0;
        $bookingHotel->children = 0;
        $bookingHotel->save();
        return $bookingHotel;
    }


}