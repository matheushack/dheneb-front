<?php

namespace Dheneb\Services\Channels\Entities\Requests\Hotel;

use \Dheneb\Services\Channels\AvailableRule;
/**
 * Class BookingHotelDetailRequest
 * @package Dheneb\Services\Channels\Entities\Requests\Hotel
 */
class BookingHotelDetailRequest
{

    /**
     * @var
     */
    private $id;

    /**
     * @var
     */
    private $room_id;

    private $bookingHotelAvailableRequest;

    /**
     * @return mixed
     */
    public function getId()
    {
        return AvailableRule::separatedId($this->id);
    }

    /**
     * @param mixed $id
     * @return BookingHotelDetailRequest
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoomId()
    {
        return $this->room_id;
    }

    /**
     * @param mixed $room_id
     * @return BookingHotelDetailRequest
     */
    public function setRoomId($room_id)
    {
        $this->room_id = $room_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return AvailableRule::separatedProvider($this->id);
    }

    /**
     * @return mixed
     */
    public function getBookingHotelAvailableRequest()
    {
        return AvailableRule::separatedMainRequest($this->id);
    }

}