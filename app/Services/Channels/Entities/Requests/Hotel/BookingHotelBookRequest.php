<?php

namespace Dheneb\Services\Channels\Entities\Requests\Hotel;

use \Dheneb\Services\Channels\AvailableRule;
use Dheneb\Services\Channels\Entities\Requests\Hotel\Tags\PassengerItem;

class BookingHotelBookRequest
{

    /**
     * @var
     */
    private $id;

    private $name;

    private $lastName;

    private $email;

    private $phone;

    private $passengers = [];

    private $locatorCode;

    /**
     * @return mixed
     */
    public function getId()
    {
        return AvailableRule::separatedId($this->id);
    }

    /**
     * @param mixed $id
     * @return BookingHotelDetailRequest
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return AvailableRule::separatedProvider($this->id);
    }

    /**
     * @return mixed
     */
    public function getBookingHotelAvailableRequest()
    {
        return AvailableRule::separatedMainRequest($this->id);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return BookingHotelBookRequest
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     * @return BookingHotelBookRequest
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return BookingHotelBookRequest
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return BookingHotelBookRequest
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return array
     */
    public function getPassengers()
    {
        return $this->passengers;
    }

    /**
     * @param array $passengers
     * @return BookingHotelBookRequest
     */
    public function setPassengers(PassengerItem $passengers)
    {
        $this->passengers[] = $passengers;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocatorCode()
    {
        return $this->locatorCode;
    }

    /**
     * @param mixed $locatorCode
     * @return BookingHotelBookRequest
     */
    public function setLocatorCode($locatorCode)
    {
        $this->locatorCode = $locatorCode;
        return $this;
    }



}