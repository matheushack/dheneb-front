<?php

namespace Dheneb\Services\Channels\Entities\Requests\Hotel;

class BookingHotelRoomAvailableRequest
{

    private $adults = 1;

    private $child = 0;

    private $age = [];


    /**
     * Get the value of Adults
     *
     * @return mixed
     */
    public function getAdults()
    {
        return $this->adults;
    }

    /**
     * Set the value of Adults
     *
     * @param mixed adults
     *
     * @return self
     */
    public function setAdults($adults)
    {
        $this->adults = $adults;

        return $this;
    }

    /**
     * Get the value of Child
     *
     * @return mixed
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * Set the value of Child
     *
     * @param mixed child
     *
     * @return self
     */
    public function setChild($child)
    {
        $this->child = $child;

        return $this;
    }

    /**
     * Get the value of Age
     *
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set the value of Age
     *
     * @param mixed age
     *
     * @return self
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

}
