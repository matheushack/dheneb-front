<?php

namespace Dheneb\Services\Channels\Entities\Response\Hotel;

use Illuminate\Support\Collection;

class BookingHotelAvailableResponse
{

    public $total;

    public $data;

    public function __construct()
    {
        $this->data = new Collection;
    }

    /**
     * Get the value of Data
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set the value of Data
     *
     * @param mixed data
     *
     * @return self
     */
    public function setData(Collection $data)
    {
        $this->data = $data;
        $this->setTotal($data->count());
        return $this;
    }


    /**
     * Get the value of Total
     *
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set the value of Total
     *
     * @param mixed total
     *
     * @return self
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

}
