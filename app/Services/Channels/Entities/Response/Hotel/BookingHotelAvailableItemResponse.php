<?php

namespace Dheneb\Services\Channels\Entities\Response\Hotel;

/**
 * Class BookingHotelAvailableItemResponse
 * @package Dheneb\Services\Channels\Entities\Response\Hotel
 */
/**
 * Class BookingHotelAvailableItemResponse
 * @package Dheneb\Services\Channels\Entities\Response\Hotel
 */
/**
 * Class BookingHotelAvailableItemResponse
 * @package Dheneb\Services\Channels\Entities\Response\Hotel
 */
class BookingHotelAvailableItemResponse
{

    /**
     * @var
     */
    public $id;

    /**
     * @var
     */
    public $hotelName;

    /**
     * @var
     */
    public $description;

    /**
     * @var
     */
    public $taxes;

    /**
     * @var
     */
    public $address;

    /**
     * @var
     */
    public $neighborhood;

    /**
     * @var
     */
    public $postalCode;

    /**
     * @var
     */
    public $category;

    /**
     * @var
     */
    public $stars;

    /**
     * @var
     */
    public $longitude;

    /**
     * @var
     */
    public $latitude;

    /**
     * @var
     */
    public $URLThumb;

    /**
     * @var
     */
    public $imagens;

    /**
     * @var
     */
    public $currency;

    /**
     * @var
     */
    public $lowerPrice;

    /**
     * @var array
     */
    public $rooms = [];

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return BookingHotelAvailableItemResponse
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHotelName()
    {
        return $this->hotelName;
    }

    /**
     * @param mixed $hotelName
     * @return BookingHotelAvailableItemResponse
     */
    public function setHotelName($hotelName)
    {
        $this->hotelName = $hotelName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return BookingHotelAvailableItemResponse
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaxes()
    {
        return $this->taxes;
    }

    /**
     * @param mixed $taxes
     * @return BookingHotelAvailableItemResponse
     */
    public function setTaxes($taxes)
    {
        $this->taxes = $taxes;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $hotelAddress
     * @return BookingHotelAvailableItemResponse
     */
    public function setAddress($hotelAddress)
    {
        $this->address = $hotelAddress;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
     * @param mixed $neighborhood
     * @return BookingHotelAvailableItemResponse
     */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     * @return BookingHotelAvailableItemResponse
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return BookingHotelAvailableItemResponse
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStars()
    {
        return $this->stars;
    }

    /**
     * @param mixed $stars
     * @return BookingHotelAvailableItemResponse
     */
    public function setStars($stars)
    {
        $this->stars = $stars;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     * @return BookingHotelAvailableItemResponse
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     * @return BookingHotelAvailableItemResponse
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getURLThumb()
    {
        return $this->URLThumb;
    }

    /**
     * @param mixed $URLThumb
     * @return BookingHotelAvailableItemResponse
     */
    public function setURLThumb($URLThumb)
    {
        $this->URLThumb = $URLThumb;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagens()
    {
        return $this->imagens;
    }

    /**
     * @param mixed $imagens
     * @return BookingHotelAvailableItemResponse
     */
    public function setImagens($imagens)
    {
        $this->imagens = $imagens;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     * @return BookingHotelAvailableItemResponse
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLowerPrice()
    {
        return $this->lowerPrice;
    }

    /**
     * @param mixed $lowerPrice
     * @return BookingHotelAvailableItemResponse
     */
    public function setLowerPrice($lowerPrice)
    {
        $this->lowerPrice = $lowerPrice;
        return $this;
    }

    /**
     * @return array
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @param array $rooms
     * @return BookingHotelAvailableItemResponse
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;
        return $this;
    }

}