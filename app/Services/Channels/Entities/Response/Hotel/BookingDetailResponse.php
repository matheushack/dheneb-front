<?php

namespace Dheneb\Services\Channels\Entities\Response\Hotel;


/**
 * Class BookingDetailResponse
 * @package Dheneb\Services\Channels\Entities\Response\Hotel
 */
class BookingDetailResponse
{

    /**
     * @var
     */
    public $id;

    /**
     * @var
     */
    public $taxes;

    /**
     * @var
     */
    public $irrevocableGuarantee;

    /**
     * @var
     */
    public $breakfastIncluded;

    /**
     * @var
     */
    public $acceptsDirectPayment;

    /**
     * @var
     */
    public $policiesCancellations;

    /**
     * @var
     */
    public $policiesConsiderations;

    /**
     * @var
     */
    public $currency;

    /**
     * @var
     */
    public $totalPrice;

    /**
     * @var
     */
    public $totalPriceWithTax;

    /**
     * @var
     */
    public $cancellationDeadline;

    /**
     * @var
     */
    public $cancellationFee;

    /**
     * @var
     */
    public $rooms = [];

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return BookingDetailResponse
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaxes()
    {
        return $this->taxes;
    }

    /**
     * @param mixed $taxes
     * @return BookingDetailResponse
     */
    public function setTaxes($taxes)
    {
        $this->taxes = $taxes;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIrrevocableGuarantee()
    {
        return $this->irrevocableGuarantee;
    }

    /**
     * @param mixed $irrevocableGuarantee
     * @return BookingDetailResponse
     */
    public function setIrrevocableGuarantee($irrevocableGuarantee)
    {
        $this->irrevocableGuarantee = $irrevocableGuarantee;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBreakfastIncluded()
    {
        return $this->breakfastIncluded;
    }

    /**
     * @param mixed $breakfastIncluded
     * @return BookingDetailResponse
     */
    public function setBreakfastIncluded($breakfastIncluded)
    {
        $this->breakfastIncluded = $breakfastIncluded;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAcceptsDirectPayment()
    {
        return $this->acceptsDirectPayment;
    }

    /**
     * @param mixed $acceptsDirectPayment
     * @return BookingDetailResponse
     */
    public function setAcceptsDirectPayment($acceptsDirectPayment)
    {
        $this->acceptsDirectPayment = $acceptsDirectPayment;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPoliciesCancellations()
    {
        return $this->policiesCancellations;
    }

    /**
     * @param mixed $policiesCancellations
     * @return BookingDetailResponse
     */
    public function setPoliciesCancellations($policiesCancellations)
    {
        $this->policiesCancellations = $policiesCancellations;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPoliciesConsiderations()
    {
        return $this->policiesConsiderations;
    }

    /**
     * @param mixed $policiesConsiderations
     * @return BookingDetailResponse
     */
    public function setPoliciesConsiderations($policiesConsiderations)
    {
        $this->policiesConsiderations = $policiesConsiderations;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     * @return BookingDetailResponse
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @param mixed $totalPrice
     * @return BookingDetailResponse
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPriceWithTax()
    {
        return $this->totalPriceWithTax;
    }

    /**
     * @param mixed $totalPriceWithTax
     * @return BookingDetailResponse
     */
    public function setTotalPriceWithTax($totalPriceWithTax)
    {
        $this->totalPriceWithTax = $totalPriceWithTax;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCancellationDeadline()
    {
        return $this->cancellationDeadline;
    }

    /**
     * @param mixed $cancellationDeadline
     * @return BookingDetailResponse
     */
    public function setCancellationDeadline($cancellationDeadline)
    {
        $this->cancellationDeadline = $cancellationDeadline;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCancellationFee()
    {
        return $this->cancellationFee;
    }

    /**
     * @param mixed $cancellationFee
     * @return BookingDetailResponse
     */
    public function setCancellationFee($cancellationFee)
    {
        $this->cancellationFee = $cancellationFee;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @param mixed $rooms
     * @return BookingDetailResponse
     */
    public function setRooms($rooms)
    {
        $this->rooms[] = $rooms;
        return $this;
    }

}