<?php

namespace Dheneb\Services\Channels\Entities\Response\Hotel;


use Dheneb\Models\BookingHotel;
use Dheneb\Models\BookingStatus;

class BookingBookResponse
{

    private $id;

    private $userId;

    private $hotelId;

    private $bookingStatusId;

    private $providerId;

    private $checkin;

    private $checkout;

    private $adults;

    private $children;

    private $currency;

    private $value;

    private $value_with_tax;

    private $deadline_change;

    private $deadline_cancellation;

    private $special_requests;

    private $sensitive_information;

    private $irrevocableGuarantee;

    private $cancellationFee;

    private $rooms;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return BookingBookResponse
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     * @return BookingBookResponse
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHotelId()
    {
        return $this->hotelId;
    }

    /**
     * @param mixed $hotelId
     * @return BookingBookResponse
     */
    public function setHotelId($hotelId)
    {
        $this->hotelId = $hotelId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBookingStatusId()
    {
        return $this->bookingStatusId;
    }

    /**
     * @param mixed $bookingStatusId
     * @return BookingBookResponse
     */
    public function setBookingStatusId($bookingStatusId)
    {
        $this->bookingStatusId = $bookingStatusId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProviderId()
    {
        return $this->providerId;
    }

    /**
     * @param mixed $providerId
     * @return BookingBookResponse
     */
    public function setProviderId($providerId)
    {
        $this->providerId = $providerId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCheckin()
    {
        return $this->checkin;
    }

    /**
     * @param mixed $checkin
     * @return BookingBookResponse
     */
    public function setCheckin($checkin)
    {
        $this->checkin = $checkin;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCheckout()
    {
        return $this->checkout;
    }

    /**
     * @param mixed $checkout
     * @return BookingBookResponse
     */
    public function setCheckout($checkout)
    {
        $this->checkout = $checkout;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdults()
    {
        return $this->adults;
    }

    /**
     * @param mixed $adults
     * @return BookingBookResponse
     */
    public function setAdults($adults)
    {
        $this->adults = $adults;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     * @return BookingBookResponse
     */
    public function setChildren($children)
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     * @return BookingBookResponse
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return BookingBookResponse
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueWithTax()
    {
        return $this->value_with_tax;
    }

    /**
     * @param mixed $value_with_tax
     * @return BookingBookResponse
     */
    public function setValueWithTax($value_with_tax)
    {
        $this->value_with_tax = $value_with_tax;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeadlineChange()
    {
        return $this->deadline_change;
    }

    /**
     * @param mixed $deadline_change
     * @return BookingBookResponse
     */
    public function setDeadlineChange($deadline_change)
    {
        $this->deadline_change = $deadline_change;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeadlineCancellation()
    {
        return $this->deadline_cancellation;
    }

    /**
     * @param mixed $deadline_cancellation
     * @return BookingBookResponse
     */
    public function setDeadlineCancellation($deadline_cancellation)
    {
        $this->deadline_cancellation = $deadline_cancellation;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpecialRequests()
    {
        return $this->special_requests;
    }

    /**
     * @param mixed $special_requests
     * @return BookingBookResponse
     */
    public function setSpecialRequests($special_requests)
    {
        $this->special_requests = $special_requests;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSensitiveInformation()
    {
        return $this->sensitive_information;
    }

    /**
     * @param mixed $sensitive_information
     * @return BookingBookResponse
     */
    public function setSensitiveInformation($sensitive_information)
    {
        $this->sensitive_information = $sensitive_information;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIrrevocableGuarantee()
    {
        return $this->irrevocableGuarantee;
    }

    /**
     * @param mixed $irrevocableGuarantee
     * @return BookingBookResponse
     */
    public function setIrrevocableGuarantee($irrevocableGuarantee)
    {
        $this->irrevocableGuarantee = $irrevocableGuarantee;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCancellationFee()
    {
        return $this->cancellationFee;
    }

    /**
     * @param mixed $cancellationFee
     * @return BookingBookResponse
     */
    public function setCancellationFee($cancellationFee)
    {
        $this->cancellationFee = $cancellationFee;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @param mixed $rooms
     * @return BookingBookResponse
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;
        return $this;
    }

    public function toModelUpdate(BookingHotel $bookingHotel)
    {
        $bookingHotel->booking_status_id = BookingStatus::CONFIRMED;
        $bookingHotel->provider_booking_id = $this->getId();
        $bookingHotel->checkin = $this->getCheckin();
        $bookingHotel->checkout = $this->getCheckout();
        $bookingHotel->adults = $this->getAdults();
        $bookingHotel->children = $this->getChildren() == 0 ? 0 :  $this->getChildren();
        $bookingHotel->currency = $this->getCurrency();
        $bookingHotel->value = $this->getValue();
        $bookingHotel->value_with_tax = $this->getValueWithTax();
        $bookingHotel->deadline_change = $this->getDeadlineChange();
        $bookingHotel->deadline_cancellation = $this->getDeadlineCancellation();
        $bookingHotel->special_requests = $this->getSpecialRequests();
        $bookingHotel->sensitive_information = $this->getSensitiveInformation();
        return $bookingHotel;
    }


}