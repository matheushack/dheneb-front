<?php

namespace Dheneb\Services\Channels\Entities\Response\Hotel\Tags;

class RoomItem
{

    public $id;

    /**
     * @var
     */
    public $count;

    /**
     * @var
     */
    public $roomName;

    /**
     * @var
     */
    public $type;

    /**
     * @var
     */
    public $regime;

    /**
     * @var
     */
    public $regimeDescription;

    /**
     * @var
     */
    public $adults;

    /**
     * @var
     */
    public $children;

    /**
     * @var
     */
    public $currency;

    /**
     * @var
     */
    public $price;

    /**
     * @var
     */
    public $priceWithTax;

    /**
     * @var
     */
    public $offer;

    /**
     * @var
     */
    public $remarks;

    /**
     * @var bool
     */
    public $irrevocableGuarantee = false;

    /**
     * @var
     */
    private $provider;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return RoomItem
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     * @return BookingRoomAvailableItemResponse
     */
    public function setCount($count)
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoomName()
    {
        return $this->roomName;
    }

    /**
     * @param mixed $roomName
     * @return BookingRoomAvailableItemResponse
     */
    public function setRoomName($roomName)
    {
        $this->roomName = $roomName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return BookingRoomAvailableItemResponse
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegime()
    {
        return $this->regime;
    }

    /**
     * @param mixed $regime
     * @return BookingRoomAvailableItemResponse
     */
    public function setRegime($regime)
    {
        $this->regime = $regime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegimeDescription()
    {
        return $this->regimeDescription;
    }

    /**
     * @param mixed $regimeDescription
     * @return BookingRoomAvailableItemResponse
     */
    public function setRegimeDescription($regimeDescription)
    {
        $this->regimeDescription = $regimeDescription;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdults()
    {
        return $this->adults;
    }

    /**
     * @param mixed $adults
     * @return BookingRoomAvailableItemResponse
     */
    public function setAdults($adults)
    {
        $this->adults = $adults;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     * @return BookingRoomAvailableItemResponse
     */
    public function setChildren($children)
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     * @return BookingRoomAvailableItemResponse
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return BookingRoomAvailableItemResponse
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPriceWithTax()
    {
        return $this->priceWithTax;
    }

    /**
     * @param mixed $priceWithTax
     * @return BookingRoomAvailableItemResponse
     */
    public function setPriceWithTax($priceWithTax)
    {
        $this->priceWithTax = $priceWithTax;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param mixed $offer
     * @return BookingRoomAvailableItemResponse
     */
    public function setOffer($offer)
    {
        $this->offer = $offer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * @param mixed $remarks
     * @return BookingRoomAvailableItemResponse
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIrrevocableGuarantee()
    {
        return $this->irrevocableGuarantee;
    }

    public function getIrrevocableGuarantee()
    {
        return $this->irrevocableGuarantee;
    }

    /**
     * @param bool $irrevocableGuarantee
     * @return BookingRoomAvailableItemResponse
     */
    public function setIrrevocableGuarantee($irrevocableGuarantee)
    {
        $this->irrevocableGuarantee = $irrevocableGuarantee;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param mixed $provider
     * @return BookingRoomAvailableItemResponse
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
        return $this;
    }



}