<?php

namespace Dheneb\Services\Channels\Entities\Response\Hotel\Tags;


/**
 * Class PassengerItem
 * @package Dheneb\Services\Channels\Entities\Response\Hotel\Tags
 */
class PassengerItem
{

    /**
     * @var
     */
    private $id;

    /**
     * @var string
     */
    private $type = 'AD';

    /**
     * @var
     */
    private $age;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return PassengerItem
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return PassengerItem
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     * @return PassengerItem
     */
    public function setAge($age)
    {
        $this->age = $age;
        return $this;
    }

}