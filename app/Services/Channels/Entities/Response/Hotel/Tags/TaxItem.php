<?php

namespace Dheneb\Services\Channels\Entities\Response\Hotel\Tags;


class TaxItem
{

    public $tax;

    public $description;

    public $value;

    public $isPercentage;

}