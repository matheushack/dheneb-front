<?php

namespace Dheneb\Services\Channels\Entities\Response\Hotel;

use Illuminate\Support\Collection;

/**
 * Class BookingRoomAvailableResponse
 * @package Dheneb\Services\Channels\Entities\Response\Hotel
 */
class BookingRoomAvailableResponse
{

    /**
     * @var
     */
    public $id;

    /**
     * @var
     */
    public $totalRoomsPrice;

    /**
     * @var
     */
    public $totalRoomsPriceWithTax;

    /**
     * @var
     */
    public $irrevocableGuarantee;

    /**
     * @var Collection
     */
    public $data;

    /**
     * BookingRoomAvailableResponse constructor.
     */
    public function __construct()
    {
        $this->data = new Collection;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return BookingRoomAvailableResponse
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalRoomsPrice()
    {
        return $this->totalRoomsPrice;
    }

    /**
     * @param mixed $totalRoomsPrice
     * @return BookingRoomAvailableResponse
     */
    public function setTotalRoomsPrice($totalRoomsPrice)
    {
        $this->totalRoomsPrice = $totalRoomsPrice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalRoomsPriceWithTax()
    {
        return $this->totalRoomsPriceWithTax;
    }

    /**
     * @param mixed $totalRoomsPriceWithTax
     * @return BookingRoomAvailableResponse
     */
    public function setTotalRoomsPriceWithTax($totalRoomsPriceWithTax)
    {
        $this->totalRoomsPriceWithTax = $totalRoomsPriceWithTax;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIrrevocableGuarantee()
    {
        return $this->irrevocableGuarantee;
    }

    /**
     * @param mixed $irrevocableGuarantee
     * @return BookingRoomAvailableResponse
     */
    public function setIrrevocableGuarantee($irrevocableGuarantee)
    {
        $this->irrevocableGuarantee = $irrevocableGuarantee;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param Collection $data
     * @return BookingRoomAvailableResponse
     */
    public function setData(BookingRoomAvailableItemResponse $data)
    {
        $this->data[] = $data;
        return $this;
    }


}