<?php
namespace Dheneb\Services\Channels;

use Dheneb\Models\ProviderChannel;

/**
 * Class ChannelService
 * @package Dheneb\Services\Channels
 */
class ChannelService
{

    /**
     * @var ProviderChannel
     */
    private $providerChannel;

    /**
     * ChannelService constructor.
     * @param ProviderChannel $providerChannel
     */
    public function __construct(ProviderChannel $providerChannel)
    {
        $this->providerChannel = $providerChannel;
    }

    public function listHotelProviders()
    {
        return $this->providerChannel
            ->where('enable', true)
            ->where('line_bussine_id', 2)
            ->get();
    }

    public function listProviders()
    {
        return $this->providerChannel->all();
    }

    public function listWithPagination($perPage = 15)
    {
        return $this->providerChannel->paginate($perPage);
    }

    public function enableProvider(int $provider_id)
    {
        $channelProvider = $this->providerChannel->find($provider_id);
        if ($channelProvider){
            $channelProvider->enable = $channelProvider->enable == true ? false : true;
            $channelProvider->save();
            return true;
        } else {
            return false;
        }
    }

    public function callProvider(ProviderChannel $providerChannel)
    {
        $providerClass = $providerChannel->provider_class;
        if (class_exists($providerClass)) {
            return new $providerClass;
        }
    }

}
