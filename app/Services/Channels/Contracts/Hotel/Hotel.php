<?php
namespace Dheneb\Services\Channels\Contracts\Hotel;

use Dheneb\Services\Channels\Contracts\Provider;
use Dheneb\Services\Channels\Entities\Requests\Hotel\{
    BookingHotelAvailableRequest
};

interface Hotel extends Provider
{

    public function availables(BookingHotelAvailableRequest $bookingHotelAvailableRequest);

//    public function detail();

}
