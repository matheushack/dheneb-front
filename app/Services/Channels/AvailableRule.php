<?php

namespace Dheneb\Services\Channels;

use Dheneb\Models\ProviderChannel;
use Crypt;
use Dheneb\Services\Channels\Entities\Requests\Hotel\BookingHotelAvailableRequest;

class AvailableRule
{

    /**
     * @param $id
     * @param ProviderChannel $providerChannel
     * @param BookingHotelAvailableRequest $bookingHotelAvailableRequest
     * @return string
     */
    static function idUnionProvider($id, ProviderChannel $providerChannel, BookingHotelAvailableRequest $bookingHotelAvailableRequest)
    {
        return Crypt::encrypt((string)json_encode([
            'id' => $id,
            'provider' => serialize($providerChannel),
            'mainRequest' => serialize($bookingHotelAvailableRequest)
        ]));
    }

    /**
     * @param $id
     * @return mixed
     */
    static function separatedProvider($id)
    {
        return unserialize(json_decode(Crypt::decrypt($id))->provider);
    }

    /**
     * @param $id
     * @return mixed
     */
    static function separatedId($id)
    {
        return json_decode(Crypt::decrypt($id))->id;
    }

    /**
     * @param $id
     * @return mixed
     */
    static function separatedMainRequest($id)
    {
        return unserialize(json_decode(Crypt::decrypt($id))->mainRequest);
    }

}