<?php

namespace Dheneb\Services;

use Dheneb\Models\LineBusiness;

class LineBusinessService
{

    /**
     * @var LineBusiness
     */
    private $lineBusiness;

    public function __construct(LineBusiness $lineBusiness)
    {
        $this->lineBusiness = $lineBusiness;
    }

    public function actives()
    {
        return $this->lineBusiness
            ->active()
            ->get();
    }

    public function checkProduct($product)
    {
        return $this->lineBusiness->active()->where('description', $product)->count() >= 1 ? true : false;
    }

}