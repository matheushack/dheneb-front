<?php

namespace Dheneb\Services;

use Dheneb\Models\HotelPhoto;

class HotelPhotoService
{

    /**
     * @var HotelPhoto
     */
    private $hotelPhoto;

    function __construct(HotelPhoto $hotelPhoto)
    {

        $this->hotelPhoto = $hotelPhoto;
    }

    public function insert(HotelPhoto $hotelPhoto)
    {
        $save = $hotelPhoto->save();
        if ($save){
            return $save;
        }
        return false;
    }

}