<?php

namespace Dheneb\Services;

use Dheneb\Models\Hotel;
use Dheneb\Models\HotelPhoto;
use Dheneb\Services\HotelService;
use Dheneb\Services\HotelPhotoService;

class MappingService
{

    const END_POINT = 'http://commons.t4w.com.br/new/api/v1/mapping/hotel/__ID__/token/1196ec7a-e3e2-45f6-811d-d528e04d218d';

    public function get($id)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, str_replace('__ID__', $id, self::END_POINT));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $hotelInfo = curl_exec($ch);
        if (!empty($hotelInfo) && $hotelInfo != 'null') {
            $hotelInfo = json_decode($hotelInfo);
            return [
                'hotel' => $this->toHotelModel($hotelInfo),
                'hotelPhotos' => $this->toHotelPhotoModel($hotelInfo)
            ];
        }
        return false;
    }

    private function toHotelModel($hotelInfo)
    {

        if (Hotel::find($hotelInfo->id)) {
            $hotel = Hotel::find($hotelInfo->id);
        } else {
            $hotel = new Hotel;
        }
        $hotel->id = $hotelInfo->id;
        $hotel->city_id = 1;
            //$hotelInfo->destination->id;
        $hotel->name = $hotelInfo->name;
        $hotel->address = $hotelInfo->address;
        $hotel->phone1 = $hotelInfo->phone1;
        $hotel->phone2 = $hotelInfo->phone2;
        $description = (array)$hotelInfo->description;
        $hotel->description_en = @$description['en'];
        $hotel->description_es = @$description['es'];
        $hotel->description_pt = @$description['pt-BR'];
        $hotel->stars = $hotelInfo->category;
        $hotel->zip_code = $hotelInfo->zipCode;
        $hotel->url_thumb = $hotelInfo->urlThumb;
        $hotel->web_site = $hotelInfo->webSite;
        $hotel->latitude = @$hotelInfo->localization->latitude;
        $hotel->longitude = @$hotelInfo->localization->longitude;
        return $hotel;
    }

    private function toHotelPhotoModel($hotelInfo)
    {
        $photos = $hotelInfo->photos;
        $hotelPhotos = [];
        foreach ($photos as $photo){
            if (HotelPhoto::where('url', $photo->url)->count() == 1) {
                $hotelPhoto = HotelPhoto::where('url', $photo->url)->first();
            } else {
                $hotelPhoto = new HotelPhoto;
            }
            $hotelPhoto->url = $photo->url;
            $description = (array)$photo->description;
            $hotelPhoto->description_en = @$description['en'];
            $hotelPhoto->description_es = @$description['es'];
            $hotelPhoto->description_pt = @$description['pt-BR'];
            $hotelPhotos[] = $hotelPhoto;
            unset($hotelPhoto);
        }
        return $hotelPhotos;
    }

    public function getAndInsert($id)
    {
        $models = $this->get($id);

        if (is_array($models)) {
            $hotelService = app(HotelService::class);
            $hotelPhotoService = app(HotelPhotoService::class);
            $hotel = $hotelService->insert($models['hotel']);

            if($hotel instanceof Hotel) {
                foreach ($models['hotelPhotos'] as $hotelPhoto){
                    $hotelPhoto->hotel_id = $hotel->id;
                    $hotelPhotoService->insert($hotelPhoto);
                }
            }
            return $hotel;
        }
        return null;
    }

}