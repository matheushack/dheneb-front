<?php

namespace Dheneb\Services;

use Dheneb\Models\Hotel;
use Illuminate\Support\Collection;

class HotelService
{

    /**
     * @var Hotel
     */
    private $hotel;

    function __construct(Hotel $hotel)
    {
        $this->hotel = $hotel;
    }

    private function query()
    {
        return $this->hotel
            ->joinCity()
            ->select(
                'hotels.*',
                'cities.city_name_pt',
                'cities.city_name_en',
                'cities.city_name_es'
            );
    }

    public function listAll()
    {
        return $this->query()->all();
    }

    public function listWithPagination($perPage = 15)
    {
        return $this->query()->paginate($perPage);
    }

    public function show($id)
    {
        try {
            return $this->query()->findOrFail($id);
        } catch(ModelNotFoundException $e){
            return [];
        }
    }

    public function insert(Hotel $hotel)
    {
        $save = $hotel->save();
        if ($save){
            return $hotel;
        }
        return false;
    }



}