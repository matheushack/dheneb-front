<?php
namespace Dheneb\Services;

use Dheneb\Models\User;
use Illuminate\Support\Collection;

class UserService
{
	
	private $user;
	
	public function __construct(User $user)
	{
		$this->user = $user;
	}
	
	private function getMenuPerPerfil()
	{
		switch ($this->user->user_perfil_id) {
			case 4:
				return new Collection(config('menu.provider'));
			break;
            case 2:
                return new Collection(config('menu.operator'));
            break;
		}
		
	}
	
	public function getMenu()
	{
		return $this->getMenuPerPerfil();
	}
	
}