<?php
namespace Dheneb\Services;

use Dheneb\Models\User;
use Illuminate\Support\Collection;
use Dheneb\Models\BookingHotel;
use Dheneb\Models\BookingTour;
use DB;

/**
 * Class BookingService
 * @package Dheneb\Services
 */
class BookingService
{

    /**
     * @var BookingHotel
     */
    private $bookingHotel;

    /**
     * @var BookingTour
     */
    private $bookingTour;

    /**
     * BookingService constructor.
     * @param BookingHotel $bookingHotel
     * @param BookingTour $bookingTour
     */
    function __construct(BookingHotel $bookingHotel, BookingTour $bookingTour)
	{
		$this->bookingHotel = $bookingHotel;
		$this->bookingTour = $bookingTour;
	}

    /**
     * @return mixed
     */
    public function getAllDetail()
	{
		$hotel = $this->bookingHotel->select([
			DB::raw(" 'hotel' as product "),
			'id',
			'user_id',
			'booking_status_id'
		]);

		$tour = $this->bookingTour->select([
			DB::raw(" 'tour' as product "),
			'id',
			'user_id',
			'booking_status_id'
		]);

		return $hotel->unionAll($tour)->sharedLock();
	}

    /**
     * @param $id
     * @return mixed
     */
    public function findHotel($id)
    {
        return $this->bookingHotel->find($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findTour($id)
    {
        return $this->bookingTour->find($id);
    }

    public function storeBookingHotel(BookingHotel $bookingHotel, User $user)
    {
        $bookingHotel->user_id = $user->id;
        return $bookingHotel->save();
    }

}
