<?php
use Dheneb\Services\UserService;
use Illuminate\Support\Collection;


/*
 * Pega o Menu conforme o perfi do usuario
 */
function menu()
{
	$userService = new UserService(\Auth::User());
	$menu = $userService->getMenu();
	return !empty($menu) ? $menu : new Collection();
}

function subName($name)
{
    $pos = strpos($name, ' ');
    $ret = array();
    if($pos !== FALSE){
        $ret['name'] = substr($name, 0, $pos);
        $ret['subname'] = substr($name, $pos+1, strlen($name));
    }

    return $ret;
}