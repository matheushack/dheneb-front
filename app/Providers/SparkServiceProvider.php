<?php

namespace Dheneb\Providers;

use Laravel\Spark\Spark;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'Dheneb',
        'product' => 'Dheneb booking motor',
        'street' => 'PO Box 111',
        'location' => 'Your Town, NY 12345',
        'phone' => '555-555-5555',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = 'alebrsux@gmail.com';

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
        'alebrsux@gmail.com'
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = true;

    /**
     * Finish configuring Spark for the application.
     *
     * @return void
     */
    public function booted()
    {
        //teamm
        Spark::noAdditionalTeams();

        // Spark::useRoles([
        //     'administrator' => 'Administrator',
        //     'operator' => 'Operator',
        //     'provider' => 'Provider'
        // ]);

        //plans
        // Spark::useBraintree()
        // ->noCardUpFront()
        // ->teamTrialDays(10);

        //se coloca assim free nao tei agencia.
        Spark::freeTeamPlan()
            ->maxTeams(1)
            ->features([
                'ale'
            ]);

        // Spark::teamPlan('Basic', 'dheneb-plan-1')
        //     ->price(10)
        //     ->maxTeams(1)
        //     ->features([
        //         'ale2'
        //     ]);
        //
        // Spark::teamPlan('Pro', 'dheneb-plan-2')
        //     ->price(20)
        //     ->maxTeams(1)
        //     ->features([
        //         'ale2'
        //     ]);

    }
}
