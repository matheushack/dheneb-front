<?php

namespace Dheneb\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Dheneb\Models\City
 *
 * @property int $id
 * @property string|null $quasar_id
 * @property string $country_id
 * @property string $country
 * @property string $city_name_en
 * @property string $city_name_pt
 * @property string $city_name_es
 * @property string $type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\City whereCityNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\City whereCityNameEs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\City whereCityNamePt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\City whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\City whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\City whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\City whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\City whereQuasarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\City whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\City whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class City extends Model
{
    //
}
