<?php

namespace Dheneb\Models;

use Laravel\Spark\Team as SparkTeam;

/**
 * Dheneb\Models\Team
 *
 * @property int $id
 * @property int $owner_id
 * @property string $name
 * @property string|null $slug
 * @property string|null $photo_url
 * @property string|null $braintree_id
 * @property string|null $current_billing_plan
 * @property string|null $paypal_email
 * @property string|null $card_brand
 * @property string|null $card_last_four
 * @property string|null $extra_billing_information
 * @property \Carbon\Carbon $trial_ends_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read string $email
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Spark\Invitation[] $invitations
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Spark\LocalInvoice[] $localInvoices
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Dheneb\Models\User $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Spark\TeamSubscription[] $subscriptions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Dheneb\Models\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Team whereBraintreeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Team whereCardBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Team whereCardLastFour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Team whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Team whereCurrentBillingPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Team whereExtraBillingInformation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Team whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Team whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Team whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Team wherePaypalEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Team wherePhotoUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Team whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Team whereTrialEndsAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Team whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Team extends SparkTeam
{

}
