<?php

namespace Dheneb\Models;

use Laravel\Spark\CanJoinTeams;
use Laravel\Spark\User as SparkUser;


/**
 * Dheneb\Models\User
 *
 * @property int $id
 * @property int|null $user_perfil_id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property string|null $photo_url
 * @property bool $uses_two_factor_auth
 * @property string|null $authy_id
 * @property string|null $country_code
 * @property string|null $phone
 * @property string|null $two_factor_reset_code
 * @property int|null $current_team_id
 * @property string|null $braintree_id
 * @property string|null $current_billing_plan
 * @property string|null $paypal_email
 * @property string|null $card_brand
 * @property string|null $card_last_four
 * @property string|null $extra_billing_information
 * @property \Carbon\Carbon $trial_ends_at
 * @property string|null $last_read_announcements_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|null $current_team
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Spark\Invitation[] $invitations
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Spark\LocalInvoice[] $localInvoices
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Spark\Subscription[] $subscriptions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Dheneb\Models\Team[] $teams
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Spark\Token[] $tokens
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereAuthyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereBraintreeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereCardBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereCardLastFour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereCurrentBillingPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereCurrentTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereExtraBillingInformation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereLastReadAnnouncementsAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User wherePaypalEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User wherePhotoUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereTrialEndsAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereTwoFactorResetCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereUserPerfilId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\User whereUsesTwoFactorAuth($value)
 * @mixin \Eloquent
 */
class User extends SparkUser
{
    use CanJoinTeams;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'authy_id',
        'country_code',
        'phone',
        'card_brand',
        'card_last_four',
        'card_country',
        'billing_address',
        'billing_address_line_2',
        'billing_city',
        'billing_zip',
        'billing_country',
        'extra_billing_information',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trial_ends_at' => 'datetime',
        'uses_two_factor_auth' => 'boolean',
    ];

    public function firstName()
    {
        return data_get(subName($this->name), 'name');
    }
    public function lastName()
    {
        return data_get(subName($this->name), 'subname');
    }

}
