<?php

namespace Dheneb\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LineBusiness
 *
 * @package Dheneb\Models
 * @property int $id
 * @property string $description
 * @property int $status
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\LineBusiness active()
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\LineBusiness inactive()
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\LineBusiness whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\LineBusiness whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\LineBusiness whereStatus($value)
 * @mixin \Eloquent
 */
class LineBusiness extends Model
{

    /**
     * @var string
     */
    protected $table = 'line_bussines';

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', 0);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeInactive($query)
    {
        return $query->where('status', 1);
    }

}