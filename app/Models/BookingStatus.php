<?php

namespace Dheneb\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Dheneb\Models\BookingStatus
 *
 * @property int $id
 * @property \Dheneb\Models\BookingStatus $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Dheneb\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingStatus whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BookingStatus extends Model
{

	use BookingTrait;

    const CONFIRMED = 1;

	const CANCELED = 2;

	const WAITING_PROVIDER = 3;

	const CONFIRMED_WAITING_PAYMENT = 4;

	public function icon()
	{
		switch($this->id){
			case 1:
				return 'icone aqui active';
			break;
			case 2:
				return 'icone aqui active';
			break;
			case 3:
				return 'icone aqui active';
			break;
			case 4:
				return 'icone aqui active';
			break;
		}
	}

}
