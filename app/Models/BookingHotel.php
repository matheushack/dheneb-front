<?php

namespace Dheneb\Models;

use Dheneb\Contracts\BookingContracts;
use Illuminate\Database\Eloquent\Model;

/**
 * Dheneb\Models\BookingHotel
 *
 * @property int $id
 * @property int $user_id
 * @property int $hotel_id
 * @property int $booking_status_id
 * @property int $provider_id
 * @property string $checkin
 * @property string $checkout
 * @property int $adults
 * @property int $children
 * @property string $currency
 * @property float $value
 * @property float $value_with_tax
 * @property string|null $deadline_change
 * @property string|null $deadline_cancellation
 * @property string|null $special_requests
 * @property string|null $sensitive_information
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Dheneb\Models\Hotel $hotel
 * @property-read \Dheneb\Models\BookingStatus $status
 * @property-read \Dheneb\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereAdults($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereBookingStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereCheckin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereCheckout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereChildren($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereDeadlineCancellation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereDeadlineChange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereHotelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereSensitiveInformation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereSpecialRequests($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereValueWithTax($value)
 * @mixin \Eloquent
 * @property string $provider_booking_id
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingHotel whereProviderBookingId($value)
 */
class BookingHotel extends Model implements BookingContracts
{

	use BookingTrait;

	public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }

}
