<?php

namespace Dheneb\Models;

use Illuminate\Database\Eloquent\Model;
use Dheneb\Models\User;
use Dheneb\Models\BookingStatus;

trait BookingTrait
{

    //show user in booking
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function status()
    {
        return $this->belongsTo(BookingStatus::class, 'booking_status_id');
    }


}