<?php

namespace Dheneb\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Dheneb\Models\ProviderChannel
 *
 * @property int $id
 * @property int $provider_id
 * @property int $line_bussine_id
 * @property string $provider_class
 * @property int $enable
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\ProviderChannel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\ProviderChannel whereEnable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\ProviderChannel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\ProviderChannel whereLineBussineId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\ProviderChannel whereProviderClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\ProviderChannel whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\ProviderChannel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProviderChannel extends Model
{
    //
}
