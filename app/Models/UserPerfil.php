<?php

namespace Dheneb\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Dheneb\Models\UserPerfil
 *
 * @property int $id
 * @property string $perfil
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\UserPerfil whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\UserPerfil whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\UserPerfil wherePerfil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\UserPerfil whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class UserPerfil extends Model
{
    //
}
