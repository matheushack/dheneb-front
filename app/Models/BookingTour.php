<?php

namespace Dheneb\Models;

use Dheneb\Contracts\BookingContracts;
use Illuminate\Database\Eloquent\Model;

/**
 * Dheneb\Models\BookingTour
 *
 * @property int $id
 * @property int $localizer
 * @property string|null $localizer_prv
 * @property int $customer_id
 * @property int $company_id
 * @property int $provider_channel_id
 * @property int $user_id
 * @property int $tour_id
 * @property float $base_amount
 * @property float $additional_amount
 * @property float $tot_amount
 * @property string $currency
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $update_at
 * @property string|null $deleted_at
 * @property int|null $booking_status_id
 * @property-read \Dheneb\Models\BookingStatus|null $status
 * @property-read \Dheneb\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereAdditionalAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereBaseAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereBookingStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereLocalizer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereLocalizerPrv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereProviderChannelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereTotAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereTourId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereUpdateAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\BookingTour whereUserId($value)
 * @mixin \Eloquent
 */
class BookingTour extends Model implements BookingContracts
{

    use BookingTrait;

}
