<?php

namespace Dheneb\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Dheneb\Models\HotelPhoto
 *
 * @property int $id
 * @property int $hotel_id
 * @property string $url
 * @property string $description_en
 * @property string $description_es
 * @property string $description_pt
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\HotelPhoto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\HotelPhoto whereDescriptionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\HotelPhoto whereDescriptionEs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\HotelPhoto whereDescriptionPt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\HotelPhoto whereHotelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\HotelPhoto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\HotelPhoto whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\HotelPhoto whereUrl($value)
 * @mixin \Eloquent
 */
class HotelPhoto extends Model
{
    //
}
