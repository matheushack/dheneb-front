<?php

namespace Dheneb\Models;

use Illuminate\Database\Eloquent\Model;
use Dheneb\Models\City;

/**
 * Dheneb\Models\Hotel
 *
 * @property int $id
 * @property int $city_id
 * @property string $name
 * @property string|null $address
 * @property string|null $phone1
 * @property string|null $phone2
 * @property string|null $description_en
 * @property string|null $description_es
 * @property string|null $description_pt
 * @property int $stars
 * @property string|null $zip_code
 * @property string|null $url_thumb
 * @property string|null $web_site
 * @property string|null $latitude
 * @property string|null $longitude
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Dheneb\Models\City $city
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel joinCity()
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel whereDescriptionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel whereDescriptionEs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel whereDescriptionPt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel wherePhone1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel wherePhone2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel whereStars($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel whereUrlThumb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel whereWebSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Dheneb\Models\Hotel whereZipCode($value)
 * @mixin \Eloquent
 */
class Hotel extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function city()
    {
        return $this->hasOne(City::class);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeJoinCity($query)
    {
        return $query->join('cities', "cities.id", '=', 'city_id');
    }

}