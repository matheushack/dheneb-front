<?php

namespace Dheneb\Console\Commands;

use Illuminate\Console\Command;
use Dheneb\Services\MappingService;


class MappingHotelConsole extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hotel:mapping';
    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'Hotel Mapping';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mappingService = app(MappingService::class);
        for ($x = 1000; $x <= 99999; $x++) {
            $hotel = $mappingService->getAndInsert($x);
            $this->comment($x . ' == ' . @$hotel->name);
        }
    }


}