<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register the API routes for your application as
| the routes are automatically authenticated using the API guard and
| loaded automatically by this application's RouteServiceProvider.
|
*/

Route::group([
    'middleware' => 'auth:api'
], function () {
    //
});


Route::get('/autocomplete-cities', function(\Illuminate\Http\Request $request){
    $query = $request->get('q');
    $cities = \Dheneb\Models\City::where('city_name_pt','like','%'.$query.'%')
                ->limit(5)
                ->get();
    $parseCities = $cities->map(function($city){
        return [ 'city' => $city->city_name_pt . ', ' . $city->country ];
    });

    return response()->json($parseCities);
});