<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@show');

Route::get('/home', 'HomeController@show');

//Hotel
Route::get('/booking/hotel/availables', 'Booking\HotelAvailablesController@index');
Route::post('/booking/hotel/availables/show', 'Booking\HotelAvailablesController@showSearch');
Route::get('/booking/hotel/availables/show', function(){
    return redirect('/booking/hotel/availables');
});
Route::get('booking/hotel/availables/detail/{hotel_id}/{room_id}', 'Booking\HotelAvailablesController@detail');
Route::post('booking/hotel/availables/book', 'Booking\HotelAvailablesController@book');
Route::get('/booking/hotel/availables/show', function(){
    return redirect('/booking/hotel/availables');
});

//booking
Route::get('/bookings', 'Booking\BookingManagerController@index');
Route::get('/bookings-waiting-confirmation', 'Booking\BookingManagerController@index');
Route::get('/bookings/{product}/{id}', 'Booking\BookingManagerController@detail');
