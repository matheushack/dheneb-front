import Autocomplete from 'vue2-autocomplete-js';
import Datepicker from 'vuejs-datepicker';

/*
 |--------------------------------------------------------------------------
 | Laravel Spark Bootstrap
 |--------------------------------------------------------------------------
 |
 | First, we will load all of the "core" dependencies for Spark which are
 | libraries such as Vue and jQuery. This also loads the Spark utilities
 | for things such as HTTP calls, forms, and form validation errors.
 |
 | Next, we'll create the root Vue application for Spark. This will start
 | the entire application and attach it to the DOM. Of course, you may
 | customize this script as you desire and load your own components.
 |
 */

require('spark-bootstrap');

require('./components/bootstrap');

var app = new Vue({
    mixins: [require('spark')]
});


//UTILITARIOS
Vue.component('autocomplete', Autocomplete);
Vue.component('datepicker', Datepicker);


//TELAS
Vue.component('hotel-availables', require('./components/HotelAvailables.vue'));
Vue.component('hotel-availables-show', require('./components/HotelAvailablesShow.vue'));
Vue.component('booking-manage', require('./components/BookingManage.vue'));
