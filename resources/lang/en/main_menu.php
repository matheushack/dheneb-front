<?php
return [

    //Reserva.
    'bookings' => 'Bookings',
    'manage' => 'Manage',
    'waiting_confirmation' => 'Waiting confirmation',
    'history' => 'History',
    'new_booking' => 'New bookings',

    //Acordo.
    'according' => 'According',
    'operator' => 'Operator',
    'brokers' => 'Brokers',

    //Tour
    'reservar' => 'Reservar',
    'tariff' => 'Tariff',

    //Usuarios
    'users' => 'Users',
];