<?php

return [


    'exemple' => 'Ex: Miami, Londres, etc...',
    'destination' => 'Destination',
    'checkin' => 'Check-in',
    'checkout' => 'Check-out',
    'nights' => 'Nights',
    'night' => 'Night',
    'rooms' => 'Rooms',
    'room' => 'Room',
    'adults' => 'Adults',
    'children' => 'Children',
    'child' => 'Child',
    'age' => 'Age',

    'search' => 'Search',

    'search_history' => 'Search History',
    

];
