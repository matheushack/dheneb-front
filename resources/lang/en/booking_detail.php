<?php
return [

    //tiitles
    'basic_information' => 'Basic Information',
    'agency_information' => 'Agency information',
    'hotel_information' => 'Hotel information',
    'passengers' => 'Passengers',

    //contents
    'passengers' => 'Passengers',
    'adults' => 'Adults',
    'children' => 'Children',
    'currency' => 'Currency',
    'value' => 'Value',
    'value_with_tax' => 'Value with tax',
    'company' => 'Company',
    'issuing' => 'Issuing', //emissor
    'phone' => 'Phone',
    'name_surname' => 'Full Name',
    'type' => 'Type',

];