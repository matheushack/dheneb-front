<?php
return [
    'confirmed' => 'Confirmed',
    'canceled' => 'Canceled',
    'waiting_provider' => 'Waiting provider',
    'confirmed_waiting_payment' => 'Confirmed Waiting Payment',
];
