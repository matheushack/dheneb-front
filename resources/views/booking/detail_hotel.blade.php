@extends('spark::layouts.app') @section("title") Home @endsection @section('breadcrumb')
    <h1>Home</h1>
    <ol class="breadcrumb">
        <li>
            <a href="/">
                <i class="fa fa-fw ti-home"></i> Home
            </a>
        </li>
    </ol>
@endsection @section('content')
    <section class="content p-l-r-15">
        <div class="panel panel-primary">
            <div class="panel-heading"><i class="fa fa-bed" aria-hidden="true"></i> Hotel</div>
            <div class="panel-body">
                <div class="row">

                    <div class="col-md-12">
                        <div class="pull-right">
                            <h4><strong>#{{$booking->id}} / {{$booking->created_at}}</strong></h4>
                            <span></span>
                        </div>
                    </div>

                    <!---inicio-->
                    <div class="col-md-12">
                        <div class="ibox-content">
                            <h3 class="">{{ trans('booking_detail.basic_information') }}</h3>
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr class="bg-primary">
                                    <th>ID</th>
                                    <th>Checkin</th>
                                    <th>Checkout</th>
                                    <th>{{trans('booking_detail.adults')}}</th>
                                    <th>{{trans('booking_detail.children')}}</th>
                                    <th>{{trans('booking_detail.currency')}}</th>
                                    <th>{{trans('booking_detail.value')}}</th>
                                    <th>{{trans('booking_detail.value_with_tax')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ $booking->id }}</td>
                                    <td>{{ $booking->checkin }}</td>
                                    <td>{{ $booking->checkout }}</td>
                                    <td>{{ $booking->adults }}</td>
                                    <td>{{ $booking->children }}</td>
                                    <td>{{ $booking->currency }}</td>
                                    <td>{{ $booking->value }}</td>
                                    <td>{{ $booking->value_with_tax }}</td>
                                </tr>
                                </tbody>
                            </table>

                            <h3 class="">{{ trans('booking_detail.agency_information') }}</h3>
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr class="bg-primary">
                                    <th>{{trans('booking_detail.company')}}</th>
                                    <th>{{trans('booking_detail.issuing')}}</th>
                                    <th>Email</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ $booking->user->teams->first()->name }}</td>
                                    <td>{{ $booking->user->name }}</td>
                                    <td>{{ $booking->user->email }}</td>
                                </tr>
                                </tbody>
                            </table>

                            <h3 class="">{{ trans('booking_detail.hotel_information') }}</h3>
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr class="bg-primary">
                                    <th>Hotel</th>
                                    <th>{{trans('booking_detail.phone')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>

                                    <td>{{ $booking->hotel->name }}</td>
                                    <td>{{ $booking->hotel->phone1 }}</td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="row mb20">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <h3 class="">{{trans('booking_detail.passengers')}}</h3>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr class="bg-primary">
                                            <th>{{trans('booking_detail.name_surname')}}</th>
                                            <th>{{trans('booking_detail.type')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>raul araujo filho</td>
                                            <td>Adult</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <h3 class="">Contact information</h3>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr class="bg-primary">
                                            <th>{{trans('booking_detail.name_surname')}}</th>
                                            <th>Email</th>
                                            <th>{{trans('booking_detail.phone')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>raul araujo filho</td>
                                            <td>contato@e-htl.com.br</td>
                                            <td>551131385656</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                            <div class="mb20">
                                <h3 class="">
                                    Liquidation
                                    <small><i class="fa fa-money"></i> USD</small>
                                    &nbsp;
                                </h3>
                            </div>
                            <div class="row mb20 print-col4">
                                <div class="col-xs-12 col-sm-3">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr class="bg-primary">
                                            <th colspan="2">Comisión agencia</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Comisión Agencia</td>
                                            <td class="align-right ">0.00</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-xs-12 col-sm-3">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr class="bg-primary">
                                            <th colspan="2">Agencia debe pagar</th>
                                        </tr>
                                        </thead>
                                        <tbody>


                                        <tr class="">
                                            <td>Monto Total</td>
                                            <td class="align-right ">843.44</td>
                                        </tr>


                                        <tr class="">
                                            <td>Comisión agencia</td>
                                            <td class="align-right  red-s"><span>(</span>0.00<span>)</span></td>
                                        </tr>


                                        <tr class="font-bold">
                                            <td>Total a pagar</td>
                                            <td class="align-right bt2px">843.44</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-xs-12 col-sm-3">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr class="bg-primary">
                                            <th colspan="2">Utilidad</th>
                                        </tr>
                                        </thead>
                                        <tbody>


                                        <tr class="">
                                            <td>Monto total</td>
                                            <td class="align-right ">843.44</td>
                                        </tr>


                                        <tr class="">
                                            <td>Costo total</td>
                                            <td class="align-right  red-s"><span>(</span>843.44<span>)</span></td>
                                        </tr>


                                        <tr class="">
                                            <td>Comisión total</td>
                                            <td class="align-right bt2px">0.00</td>
                                        </tr>


                                        <tr class="">
                                            <td>Comisión agencia</td>
                                            <td class="align-right  red-s"><span>(</span>0.00<span>)</span></td>
                                        </tr>


                                        <tr class="">
                                            <td>Comisión negocio</td>
                                            <td class="align-right bt2px">0.00</td>
                                        </tr>


                                        <tr class="">
                                            <td>Bono al Counter</td>
                                            <td class="align-right  red-s"><span>(</span>0.00<span>)</span></td>
                                        </tr>


                                        <tr class="">
                                            <td>Bono Adicional</td>
                                            <td class="align-right  red-s"><span>(</span>0.00<span>)</span></td>
                                        </tr>


                                        <tr class="font-bold">
                                            <td>Ganancia operativa</td>
                                            <td class="align-right bt2px">0.00</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-xs-12 col-sm-3">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr class="bg-primary">
                                            <th colspan="2">Resumen</th>
                                        </tr>
                                        </thead>
                                        <tbody>


                                        <tr class="">
                                            <td>Costo Total</td>
                                            <td class="align-right ">843.44</td>
                                        </tr>


                                        <tr class="">
                                            <td>Bono al Counter</td>
                                            <td class="align-right ">0.00</td>
                                        </tr>


                                        <tr class="">
                                            <td>Bono adicional</td>
                                            <td class="align-right ">0.00</td>
                                        </tr>


                                        <tr class="">
                                            <td>Ganancia operativa</td>
                                            <td class="align-right ">0.00</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <h3 class="">Policies</h3>
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr class="bg-primary">
                                    <th>Product</th>
                                    <th>Apply to</th>
                                    <th>Considered</th>
                                    <th>If state is in</th>
                                    <th class="align-right">Value</th>
                                    <th class="align-right">Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Hotel</td>
                                    <td>by total</td>
                                    <td>by File</td>
                                    <td>
                                        <span class="label label-RR">RR</span>
                                    </td>
                                    <td class="align-right">
                                        (USD) 25.00
                                    </td>
                                    <td class="align-right red">
                                        (USD)
                                        0.00
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="tabs-container nav-noline no-print mb20">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#BmBookComments" data-toggle="tab">Comments</a></li>
                                    <li><a href="#BmBookLogs" data-toggle="tab">Logs</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="BmBookComments">
                                        <div class="mt20">
                                            <h3 class=""><i class="icon-comment blue"></i> Add comment</h3>
                                            <form method="post" action="/en/backoffice/book/comment/137703">
                                                <div class="input-group">
                                                    <textarea name="comment" class="form-control"
                                                              placeholder="Type your message here..."></textarea>
                                                    <span class="input-group-btn ver-al-t">
                                                    <button type="submit" class="btn btn-sm btn-success"> <i
                                                                class="fa fa-plus"></i> Save</button>
                                                </span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="BmBookLogs">
                                        <div class="mt20">
                                            <ul class="sortable-list connectList agile-list ui-sortable">
                                                <li class="warning-element" id="task6">
                                                    Item number '01': Reservation in process (IP)
                                                    <div class="agile-detail">
                                                        <a href="#" class="pull-right btn btn-xs btn-primary">Xml</a>
                                                        <i class="fa fa-clock-o"></i> 23-06-2017 16:43:08
                                                    </div>
                                                </li>
                                                <li class="warning-element" id="task6">
                                                    Item number '01': Reservation completed (RR)
                                                    <div class="agile-detail">
                                                        <a href="#" class="pull-right btn btn-xs btn-primary">Xml</a>
                                                        <i class="fa fa-clock-o"></i> 23-06-2017 16:43:09
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{--<div class="yes-print">--}}
                                {{--<h3 class="">Comments</h3>--}}
                            {{--</div>--}}
                            {{--<div class="well well-sm no-print">--}}
                                {{--<a class="btn btn-primary" href="/en/backoffice/book/assign/137703" data-placement="top"--}}
                                   {{--rel="tooltip"> Assign me</a>--}}
                                {{--<div class="btn-group"><a class="btn btn-primary"--}}
                                                          {{--href="/en/backoffice/book-report/liquidation/137703"--}}
                                                          {{--data-placement="top" rel="tooltip" target="_blank"> View--}}
                                        {{--liquidation</a>--}}
                                    {{--<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"--}}
                                            {{--aria-haspopup="true" aria-expanded="false">--}}
                                        {{--<b class="fa fa-caret-up"></b></button>--}}
                                    {{--<ul class="dropdown-menu">--}}
                                        {{--<li><a class="btn btn-primary"--}}
                                               {{--href="/en/backoffice/book-report/pre-send-liquidation/137703"--}}
                                               {{--data-placement="top" rel="tooltip"> Send liquidation</a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                {{--<a class="btn btn-primary"--}}
                                   {{--href="/en/backoffice/book-documents/index/137703?r=%2Fen%2Fbackoffice%2Fbook%2Fdetail%2F137703"--}}
                                   {{--data-placement="top" rel="tooltip"> Documents</a> <a class="btn btn-primary"--}}
                                                                                        {{--href="/en/backoffice/payment/view/0/137703"--}}
                                                                                        {{--data-placement="top"--}}
                                                                                        {{--rel="tooltip"> Payments</a> <a--}}
                                        {{--class="btn btn-primary" href="/en/backoffice/book/verify-payment/137703"--}}
                                        {{--data-placement="top" rel="tooltip" popup-verify-payment="true"> Verify--}}
                                    {{--payment</a> <a class="btn btn-default" href="/en//es/backoffice/book/list/page/1"--}}
                                                   {{--data-placement="top" rel="tooltip"> Back</a>--}}
                            {{--</div>--}}


                        </div>
                    </div>

                    <!---fim--->


                </div>

                <br/>
                <div class="btn-section">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <span class="pull-right">
                        <button type="button" class="btn btn-responsive button-alignment btn-danger" data-toggle="button">
                             Cancel
                        </button>
                    </span>
                    </div>
                </div>


            </div>
        </div>

    </section>

@endsection
