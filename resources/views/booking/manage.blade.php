
@extends('spark::layouts.app') @section("title") Home @endsection @section('breadcrumb')
<h1>Home</h1>
<ol class="breadcrumb">
    <li>
        <a href="/">
            <i class="fa fa-fw ti-home"></i> Home
        </a>
    </li>
</ol>
@endsection @section('content')
<booking-manage inline-template>
    <div>

        <div id="show_bookings">
            <table class="table table-striped table-bordered" id="table1" style="cursor: pointer;">
                <thead>
                <tr>
                    <th width="5%">{{ trans('listing.booking_id') }}</th>
                    <th width="20%">{{ trans('listing.product') }}</th>
                    <th width="30%">{{ trans('listing.issuer') }}</th>
                    <th width="20%">{{ trans('global.' . Spark::teamString()) }}</th>
                    <th width="20%">{{ trans('listing.status') }}</th>
                    <th width="5%">{{ trans('listing.actions') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($bookings as $booking)
                        <tr>
                            <td tabindex="1">
                                {{ $booking->id }}
                            </td>
                            <td tabindex="1">
                                {{ trans('products.' . $booking->product) }}
                            </td>
                            <td tabindex="1">
                                {{ $booking->user->name }}
                            </td>
                            <td tabindex="1">
                                {{ $booking->user->teams->first()->name }}
                            </td>
                            <td tabindex="1">
                                {{ trans('booking_status.' . $booking->status->status) }}
                            </td>
                            <td tabindex="1">
                                <a class="btn btn-labeled btn-primary" href="/bookings/{{$booking->product}}/{{$booking->id}}">
                                    <span class="btn-label">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


</booking-manage>
@endsection
