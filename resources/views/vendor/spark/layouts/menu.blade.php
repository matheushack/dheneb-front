@foreach (menu() as $main => $menu)
	@if (count($menu['sub']) >= 1)
		<li class="menu-dropdown">
			<a href="javascript:void(0)">
			<i class="menu-icon {{ $menu['icon'] }}"></i>
			<span class="mm-text">{{ trans($main) }}</span>
			<span class="fa arrow"></span>
	@else
		<li>
			<a href="/{{ $menu['href'] }}">
			<i class="menu-icon {{ $menu['icon'] }}"></i>
			<span class="mm-text">{{ trans($main) }}</span>
	@endif

		</a>
	@if (count($menu['sub']) >= 1)
		@foreach ($menu['sub'] as $mainsub => $sub)
			<ul>
				<li>
					<a href="/{{ $sub['href'] }}" aria-controls="{{ strtolower(trans($mainsub)) }}">
						<i class="fa fa-fw fa-btn {{ $sub['icon'] }}"></i>{{ trans($mainsub) }}
					</a>
				</li>
			</ul>
		@endforeach
	@endif
	</li>
@endforeach

@if (Spark::usesTeams())
<li class="menu-dropdown"><a href="#"><i class="menu-icon fa fa-cogs"></i><span> {{ ucfirst(str_plural(Spark::teamString())) }} Settings</span><span class="fa arrow"></span></a>
    <ul>
        <li v-for="team in teams">
            <a :href="'/settings/{{str_plural(Spark::teamString())}}/'+team.id">
                <span>
    				<img :src="team.photo_url" class="spark-team-photo-xs"><i class="fa fa-btn"></i><span> @{{ team.name }}</span>
                </span>
            </a>
        </li>
    </ul>
</li>
@endif

@if (Spark::developer(Auth::user()->email))
	<li class="menu-dropdown">
	    <a href="javascript:void(0)">
	        <i class="menu-icon fa fa-terminal"></i>
	        <span>Developer</span>
	        <span class="fa arrow"></span>
	    </a>
	    <ul>
	        <!-- Announcements Link -->
	        <li>
	            <a href="/spark/kiosk#/announcements">
	                <i class="fa fa-fw fa-btn fa-bullhorn"></i>Announcements
	            </a>
	        </li>
	        <!-- Metrics Link -->
	        <li>
	            <a href="/spark/kiosk#/metrics">
	                <i class="fa fa-fw fa-btn fa-bar-chart"></i>Metrics
	            </a>
	        </li>
	        <!-- Users Link -->
	        <li>
	            <a href="/spark/kiosk#/users">
	                <i class="fa fa-fw fa-btn fa-user"></i>Users
	            </a>
	        </li>
	    </ul>
	</li>
@endif


@if (Spark::usesTeams() && (Spark::createsAdditionalTeams() || Spark::showsTeamSwitcher()))
	<hr/>
	@include('spark::nav.teams')
@endif
