<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title', config('app.name'))</title>
    <!-- favcon -->
    <link rel="shortcut icon" href="/img/favicon.ico" />
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
    <!-- CSS -->
    <link href="/css/sweetalert.css" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="/css/themify-icons.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">


    @yield('css', '')
    <style type="text/css">
        .vdp-datepicker {
            position: relative;
            text-align: left
        }

        .vdp-datepicker * {
            box-sizing: border-box
        }

        .vdp-datepicker__calendar {
            position: absolute;
            z-index: 100;
            background: #fff;
            width: 300px;
            border: 1px solid #ccc
        }

        .vdp-datepicker__calendar header {
            display: block;
            line-height: 40px
        }

        .vdp-datepicker__calendar header span {
            display: inline-block;
            text-align: center;
            width: 71.42857142857143%;
            float: left
        }

        .vdp-datepicker__calendar header .next,
        .vdp-datepicker__calendar header .prev {
            width: 14.285714285714286%;
            float: left;
            text-indent: -10000px;
            position: relative
        }

        .vdp-datepicker__calendar header .next:after,
        .vdp-datepicker__calendar header .prev:after {
            content: "";
            position: absolute;
            left: 50%;
            top: 50%;
            -webkit-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
            border: 6px solid transparent
        }

        .vdp-datepicker__calendar header .prev:after {
            border-right: 10px solid #000;
            margin-left: -5px
        }

        .vdp-datepicker__calendar header .prev.disabled:after {
            border-right: 10px solid #ddd
        }

        .vdp-datepicker__calendar header .next:after {
            border-left: 10px solid #000;
            margin-left: 5px
        }

        .vdp-datepicker__calendar header .next.disabled:after {
            border-left: 10px solid #ddd
        }

        .vdp-datepicker__calendar header .next:not(.disabled),
        .vdp-datepicker__calendar header .prev:not(.disabled),
        .vdp-datepicker__calendar header .up:not(.disabled) {
            cursor: pointer
        }

        .vdp-datepicker__calendar header .next:not(.disabled):hover,
        .vdp-datepicker__calendar header .prev:not(.disabled):hover,
        .vdp-datepicker__calendar header .up:not(.disabled):hover {
            background: #eee
        }

        .vdp-datepicker__calendar .disabled {
            color: #ddd;
            cursor: default
        }

        .vdp-datepicker__calendar .cell {
            display: inline-block;
            padding: 0 5px;
            width: 14.285714285714286%;
            height: 40px;
            line-height: 40px;
            text-align: center;
            vertical-align: middle;
            border: 1px solid transparent
        }

        .vdp-datepicker__calendar .cell:not(.blank):not(.disabled).day,
        .vdp-datepicker__calendar .cell:not(.blank):not(.disabled).month,
        .vdp-datepicker__calendar .cell:not(.blank):not(.disabled).year {
            cursor: pointer
        }

        .vdp-datepicker__calendar .cell:not(.blank):not(.disabled).day:hover,
        .vdp-datepicker__calendar .cell:not(.blank):not(.disabled).month:hover,
        .vdp-datepicker__calendar .cell:not(.blank):not(.disabled).year:hover {
            border: 1px solid #4bd
        }

        .vdp-datepicker__calendar .cell.selected,
        .vdp-datepicker__calendar .cell.selected.highlighted,
        .vdp-datepicker__calendar .cell.selected:hover {
            background: #4bd
        }

        .vdp-datepicker__calendar .cell.highlighted {
            background: #cae5ed
        }

        .vdp-datepicker__calendar .cell.grey {
            color: #888
        }

        .vdp-datepicker__calendar .cell.grey:hover {
            background: inherit
        }

        .vdp-datepicker__calendar .cell.day-header {
            font-size: 75%;
            white-space: no-wrap;
            cursor: inherit
        }

        .vdp-datepicker__calendar .cell.day-header:hover {
            background: inherit
        }

        .vdp-datepicker__calendar .month,
        .vdp-datepicker__calendar .year {
            width: 33.333%
        }

        .vdp-datepicker__calendar-button,
        .vdp-datepicker__clear-button {
            cursor: pointer;
            font-style: normal
        }
    </style>
    <style type="text/css">
        .slide-fade-enter-active {
            transition: all .3s ease;
        }

        .slide-fade-leave-active {
            transition: all .8s cubic-bezier(1.0, 0.5, 0.8, 1.0);
        }

        .slide-fade-enter,
        .slide-fade-leave-to
            /* .slide-fade-leave-active for <2.1.8 */

        {
            -webkit-transform: translateX(10px);
            transform: translateX(10px);
            opacity: 0;
        }
    </style>
    <!-- Scripts -->
    @yield('scripts', '')
    <!-- Global Spark Object -->
    <script>
    window.Spark = <?php echo json_encode(array_merge(
            Spark::scriptVariables(), []
        )); ?>;
    </script>
</head>

<body class="skin-default">
    <div class="preloader">
        <div class="loader_img"><img src="/img/loader.gif" alt="loading..." height="64" width="64"></div>
    </div>
    <div id="spark-app" v-cloak>
        <!-- Navigation -->
        <header class="header">
            @if (Auth::check()) @include('spark::nav.user') @else @include('spark::nav.guest') @endif
        </header>
        <!-- Main Content -->
        <div class="wrapper row-offcanvas row-offcanvas-left">
            @if (Auth::check())
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar-->
                @include('spark::layouts.leftmenu')
                <!-- /.sidebar -->
            </aside>
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    @yield('breadcrumb')
                </section>
                <!-- Main content -->
                <section class="content">
                    @yield('content')
                </section>
                <!-- /.content -->
            </aside>
            @else
            <section class="content m-t-75">
                @yield('content')
            </section>
            @endif
        </div>
        <!-- Application Level Modals -->
        @if (Auth::check()) @include('spark::modals.notifications') @include('spark::modals.support') @include('spark::modals.session-expired') @endif
    </div>
    <!-- JavaScript -->

    <script src="{{ mix('js/app.js') }}"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
</body>

</html>
