<section class="sidebar">
    <div id="menu" role="navigation">
        <div class="nav_profile">
            <div class="media profile-left">
                <a class="pull-left profile-thumb" href="javascript:void(0)">
                    <img :src="user.photo_url" class="img-circle" alt="User Image">
                </a>
                <div class="content-profile">
                    <h4 class="media-heading">@{{ user.name }}</h4>
                    <ul class="icon-list">
                        <li>
                            <a href="/settings">
                                <i class="fa fa-fw ti-settings"></i>
                            </a>
                        </li>
                        <li>
                            <a href="/settings#/security">
                                <i class="fa fa-fw ti-lock"></i>
                            </a>
                        </li>
                        <li>
                            <a href="/settings#/api">
                                <i class="fa fa-fw ti-list"></i>
                            </a>
                        </li>
                        <li>
                            <a href="/logout">
                                <i class="fa fa-fw ti-shift-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="navigation">
<!--             <li> -->
<!--                 <a href="/home"> -->
<!--                     <i class="menu-icon ti-desktop"></i> -->
<!--                     <span class="mm-text ">Home</span> -->
<!--                 </a> -->
<!--             </li> -->
<!--             <li> -->
<!--                 <a href="/hotel-availables"> -->
<!--                     <i class="menu-icon ti-desktop"></i> -->
<!--                     <span class="mm-text ">Hotel Availables</span> -->
<!--                 </a> -->
<!--             </li> -->
            
            @include('spark::layouts.menu')
            
        </ul>
        <!-- / .navigation -->
    </div>
    <!-- menu -->
</section>
