@extends('spark::layouts.app')


@section('scripts')

@endsection

@section('content')

<hotel-availables :user="user" inline-template>
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Search Hotel</div>

                    <div class="panel-body">

                    {{-- <autocomplete
                          url="/api/autocomplete-cities"
                          anchor="city"
                          label="destination"
                          class="col-md-12"
                          name="destination">
                      </autocomplete> --}}

                        <form method="POST" name="search_hotel" id="search_hotel" action="/booking/hotel/availables/show">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="destiny-hotel">{{ trans('search-hotel.destination') }}</label>
                                 <autocomplete
                                    placeholder="{{trans('search-hotel.exemple')}}"
                                    url="/api/autocomplete-cities"
                                    anchor="city"
                                    label="destination"
                                    class="col-md-12"
                                    name="destination_id">
                                </autocomplete>
                                {{--<input name="searchhotel[destiny]" type="text" id="destiny-hotel" class="form-control ciudad-destino ui-autocomplete-input" placeholder="{{trans('search-hotel.exemple')}}" value="" autocomplete="off">--}}
                            </div>
                            <div class="row form-group">
                                <div class="col-md-5 checkin">
                                    <label for="checkin">{{ trans('search-hotel.checkin') }}</label>
                                    {{-- <input name="searchhotel[checkin]" type="text" id="checkin" class="form-control datepicker" value="">         --}}
                                    <datepicker
                                                input-class="form-control"
                                                name="checkin"
                                                calendar-button-icon="fa fa-calendar"
                                    ></datepicker>
                                </div>
                                <div class="col-md-2 nights">
                                    <label for="nights">{{ trans('search-hotel.nights') }}</label>
                                    <select name="nights" required="required" id="nights" class="form-control"><option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    </select>
                                </div>
                                <div class="col-md-5 checkout">
                                    <label for="checkout">{{ trans('search-hotel.checkout') }}</label>
                                    <datepicker
                                                input-class="form-control"
                                                name="checkout"
                                                calendar-button-icon="fa fa-calendar"
                                    ></datepicker>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-5 rooms">
                                    <label for="rooms">{{ trans('search-hotel.rooms') }}</label>
                                    <select name="rooms" required="required" id="rooms" class="form-control" @change="roomToPassager()" v-model="selectRooms">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group"  v-for="roominput of selectRooms">
                                    <div class="col-xs-2 room">
                                        <label for="listrooms">{{ trans('search-hotel.room') }} - @{{ roominput }}</label>
                                    </div>
                                    <div class="col-md-2 adults">
                                        <label>{{ trans('search-hotel.adults') }}</label>
                                        <select name="listrooms[1][adults]" required="required" class="form-control">
                                        <option value="1">1</option>
                                            <option value="2" selected="selected">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            </select>
                                    </div>
                                    <div class="col-md-2 child">
                                        <label>{{ trans('search-hotel.children') }}</label>
                                        <select name="listrooms[1][children]" required="required" class="form-control">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2 child">
                                        <label>{{ trans('search-hotel.age') }}</label>
                                        <select name="listrooms[1][childrenages][age]" required="required" class="form-control">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                        </select>
                                    </div>

                                </div>
                            <input class="btn ladda-button btn-primary btn-lg" name="btnSearch" type="submit" value="{{ trans('search-hotel.search') }}" />
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</hotel-availables>
@endsection
