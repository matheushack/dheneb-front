@extends('spark::layouts.app')


@section('scripts')

@endsection

@section('content')

    <div class="panel panel-default">

        <div class="panel-body">

            <form method="post" name="book-form" id="book-form" class="form-horizontal" action="/booking/hotel/availables/book?h={{ uniqid() }}">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $detail->getId() }}">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title">Políticas de cancelamento</h3>
                    </div>
                    <div class="panel-body">
                        {!! $detail->getPoliciesCancellations() !!}
                    </div>
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="collapsed">Ver mais</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" style="height: 0px;">
                                <div class="panel-body">
                                    <strong>Para cancelar você deve considerar a hora do destino.</strong>
                                    <p>
                                        A partir de <strong>{{ \Carbon\Carbon::parse($detail->getCancellationDeadline())->format('Y-m-d') }}</strong>, a <strong>23:59:00 hrs.</strong>
                                        será cobrada a taxa de  <strong>{{ $detail->getCurrency() }} {{ $detail->getCancellationFee() }}</strong>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title">Políticas de cancelamento fornecedor</h3>
                    </div>
                    <div class="panel-body">
                        <strong>Para cancelar você deve considerar a hora do destino.</strong>
                        <p>
                            A partir de <strong>{{ \Carbon\Carbon::parse($detail->getCancellationDeadline())->format('Y-m-d') }}</strong>, a <strong>23:59:00 hrs.</strong>
                            será cobrada a taxa de  <strong>{{ $detail->getCurrency() }} {{ $detail->getCancellationFee() }}</strong>
                        </p>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Remarks</h3>
                    </div>
                    <div class="panel-body">
                        {!! $detail->getPoliciesConsiderations() !!}
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Nome do responsável pela reserva</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <input name="name" id="name" type="text" class="form-control" placeholder="Nome" value="{{ $user->firstName() }}">
                            </div>
                            <div class="col-md-3">
                                <input name="lastName" id="surname" type="text" class="form-control" placeholder="Sobrenome" value="{{ $user->lastName() }}">
                            </div>
                            <div class="col-md-3">
                                <input name="email" id="email" type="email" class="form-control" placeholder="Email" pattern="[a-zA-Z0-9_+]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" value="{{ $user->email }}">
                            </div>
                            <div class="col-md-3">
                                <input name="phone" id="phone" type="text" maxlength="15" class="form-control" placeholder="Telefone" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Dados dos passageiros <span>Todos os campos são de preenchimento obrigatório</span></h3>
                    </div>
                    <div class="panel-body">
                        @foreach($detail->getRooms() as $rooms)
                            @for ($r = 0; $r < $rooms->getCount(); $r++)
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="title">
                                            <h6>Quarto [{{ ucfirst($rooms->getRoomName()) }}]</h6>
                                        </div>
                                        @for ($i = 0; $i < $rooms->getAdults(); $i++)
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>Passageiro # {{ $i + 1 }}</label>
                                                    <input name="passengers[{{ $r }}][adults][{{ $i }}][type]" type="hidden" value="AD">
                                                    <input name="passengers[{{ $r }}][adults][{{ $i }}][nationality]" type="hidden" value="72">
                                                </div>
                                                <div class="col-md-3">
                                                    <input name="passengers[{{ $r }}][adults][{{ $i }}][name]" type="text" class="form-control texthidden" placeholder="Nome" value="">
                                                </div>
                                                <div class="col-md-3">
                                                    <input name="passengers[{{ $r }}][adults][{{ $i }}][lastName]" type="text" class="form-control texthidden" placeholder="Sobrenome" value="">
                                                </div>
                                            </div>
                                            <br>
                                        @endfor
                                    </div>
                                </div>
                            @endfor
                        @endforeach
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Comentário do hotel <span>Você será assistida nos termos do contrato, a disponibilidade ea do hotel</span></h3>
                    </div>
                    <div class="panel-body">
                        <textarea name="comments" id="comments" rows="4" class="form-control"></textarea>
                    </div>
                </div>

                <div class="mb10">
                    <div class="alert alert-info">
                        <h4>Condições Gerais</h4>
                        <p>Para finalizar a reserva, por favor, aceite as Políticas da loja e pressione o botão Confirmar Reserva. Todos os seus dados pessoais serão transmitidos criptografados para garantir uma operação segura.</p>
                    </div>
                    <label>
                        <input type="hidden" name="agree" value=""><input type="checkbox" name="agree" id="agree" value="1">
                        Eu li e aceito os <a href="#modal-store-policies" class="btn-link" role="button" data-toggle="modal">Condições Gerais</a>
                    </label>
                </div>

                <input type="submit" class="btn btn-primary" name="reservar" value="Reservar">
            </form>
        </div>
    </div>


@endsection
