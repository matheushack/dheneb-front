@extends('spark::layouts.app')


@section('css')
    <link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
    <link href="/css/hint.min.css" rel="stylesheet">
    <link href="/css/hotel-avail.css" rel="stylesheet">
@endsection

@section('content')
<hotel-availables-show :user="user" inline-template>
    <aside class="right-side">
        <section class="content-header">
            <div class="col-md-12">
                <h3 class="title-search">
                    <i class="fa fa-building" aria-hidden="true"></i> Hotels in <strong>São Paulo</strong> -
                    Displaying <strong>200</strong> of <strong>1500</strong>
                </h3>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <p class="title-search-detail pull-right">
                        <i class="fa fa-calendar" aria-hidden="true"></i> Checkin <strong>2017-07-22</strong> -
                        <i class="fa fa-calendar" aria-hidden="true"></i> Checkout <strong>2017-07-23</strong> -
                        <i class="fa fa-building" aria-hidden="true"></i> Apartaments <strong>2</strong> -
                        <i class="fa fa-users" aria-hidden="true"></i> Adults <strong>2</strong> -
                        <i class="fa fa-users" aria-hidden="true"></i> Children <strong>1</strong>
                    </p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="filter">
                    <div class="map">
                        <figure>
                            <img src="https://www.smartdestinations.com/img/maps/attrs/Bos/Bos_Att_Museum_of_Science.png">
                            <figcaption><i class="fa fa-search" aria-hidden="true"></i> View all hotels in map</figcaption>
                        </figure>
                    </div>
                    <form>
                        <fieldset class="filter-title">Filter by stars</fieldset>
                        <hr>

                        <fieldset class="filter-title">Filter by price</fieldset>
                        <hr>

                        <fieldset class="filter-title">Filter by regime</fieldset>
                        <hr>

                        <fieldset class="filter-title">Filter by name</fieldset>
                        <hr>

                        <fieldset class="filter-title">Filter by service</fieldset>
                        <hr>
                    </form>

                </div>
            </div>
            <div class="col-md-9">
                @foreach ($availables->getData() as $hotelAvail)
                <div class="hotel">
                    <div class="container-fluid">
                        <div class="wrapper row">
                            <div class="photos col-md-3">
                                <div class="fotorama" data-nav="thumbs-{{ md5($hotelAvail->getId()) }}" data-keyboard="true" data-autoplay="2000">
                                    @if(!empty($hotelAvail->getURLThumb()))
                                        <a href="{{$hotelAvail->getURLThumb()}}"><img src="{{$hotelAvail->getURLThumb()}}"></a>
                                    @endif

                                    @foreach ($hotelAvail->getImagens() as $imagens)
                                            <a href="{{$imagens}}"><img src="{{$imagens}}"></a>
                                    @endforeach
                                </div>
                            </div>
                            <div class="details col-md-7">
                                <h4 class="hotel-title name">{{ $hotelAvail->getHotelName() }}</h4>
                                <div class="stars">
                                    @if($hotelAvail->getStars() > 0)
                                        @for ($i = 0; $i < $hotelAvail->getStars(); $i++)
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        @endfor
                                    @endif
                                </div>
                                <p class="address">
                                    <i class="fa fa-map-marker blue"></i> {{ $hotelAvail->getAddress() }}

                                    <a href="" class="view-map">
                                        <i class="fa fa-map" aria-hidden="true"></i> View in map
                                    </a>
                                </p>
                                <p class="hotel-description">
                                    {{ $hotelAvail->getDescription() }}
                                </p>
                                <ul class="services list-inline">
                                    <li class="hint--bottom" aria-label="Breakfast">
                                        <i class="fa fa-coffee fa-lg"></i>
                                    </li>
                                    <li class="hint--bottom" aria-label="Lunch">
                                        <i class="fa fa-cutlery fa-lg"></i>
                                    </li>
                                    <li class="hint--bottom" aria-label="Parking">
                                        <i class="fa fa-car fa-lg"></i>
                                    </li>
                                    <li class="hint--bottom" aria-label="Air conditioning">
                                        <i class="fa fa-snowflake-o fa-lg"></i>
                                    </li>
                                    <li class="hint--bottom" aria-label="Wifi">
                                        <i class="fa fa-wifi fa-lg"></i>
                                    </li>
                                    <li class="hint--bottom" aria-label="Beer">
                                        <i class="fa fa-beer fa-lg"></i>
                                    </li>
                                </ul>
                            </div>
                            <div class="prices col-md-2">
                                <div class="row">
                                    <ul class="actions list-inline pull-right">
                                        <li>
                                            <a href="" class="hint--bottom" aria-label="Favorite">
                                                <i class="fa fa-heart fa-lg"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="" class="hint--bottom" aria-label="Share">
                                                <i class="fa fa-share-alt-square fa-lg"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="box-price">
                                    <p class="text-price">Price</p>
                                    <small>Description price</small>

                                    <p class="price">{{ $hotelAvail->getCurrency() }} {{ $hotelAvail->getLowerPrice() }}</p>
                                </div>
                                <a href="" target="_blank" class="btn btn-success btn-block btn-reserve">
                                    <i aria-hidden="true" class="fa fa-shopping-cart"></i> Reserve
                                </a>
                            </div>
                        </div>
                        <div class="wrapper row view-all-rooms">
                            <div class="panel-group" id="view-rooms-{{ md5($hotelAvail->getId()) }}">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 data-toggle="collapse" data-parent="#view-rooms-{{ md5($hotelAvail->getId()) }}" href="#rooms-{{ md5($hotelAvail->getId()) }}" class="panel-title expand">
                                            <div class="right-arrow pull-right">+</div>
                                            <a href="#"><i class="fa fa-th-list" aria-hidden="true"></i> View all rooms</a>
                                        </h4>
                                    </div>
                                    <div id="rooms-{{ md5($hotelAvail->getId()) }}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr class="bg-primary">
                                                    <th><i class="fa fa-bed" aria-hidden="true"></i> Room</th>
                                                    <th><i class="fa fa-cutlery" aria-hidden="true"></i> Regime</th>
                                                    <th><i class="fa fa-users" aria-hidden="true"></i> Passagers</th>
                                                    <th><i class="fa fa-money" aria-hidden="true"></i> Price</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($hotelAvail->getRooms() as $rooms)
                                                    @foreach($rooms->getData() as $roomData)
                                                        <tr>
                                                            <td>{{ $roomData->getRoomName() }}</td>
                                                            <td>{{ $roomData->getRegime() }}</td>
                                                            <td>Adults: {{ $roomData->getAdults() }}</td>
                                                            <td align="right">{{ $roomData->getCurrency() }} {{ $roomData->getPrice() }}</td>
                                                            <td></td>
                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        <td colspan="4" align="right">{{ $hotelAvail->getCurrency() }} {{ $rooms->getTotalRoomsPrice() }}</td>
                                                        <td><a href="/booking/hotel/availables/detail/{{ $hotelAvail->getId() }}/{{ $rooms->getId() }}" class="btn btn-success btn-block" target="_blank"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Reserve</a></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </section>
    </aside>

</hotel-availables-show>
@endsection
