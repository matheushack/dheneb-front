@extends('spark::layouts.app') @section("title") Home @endsection @section('breadcrumb')
<h1>Home</h1>
<ol class="breadcrumb">
    <li>
        <a href="/">
            <i class="fa fa-fw ti-home"></i> Home
        </a>
    </li>
</ol>
@endsection @section('content')
<home :user="user" inline-template>
    <div>
        <!-- Application Dashboard -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-success">
                    <div class="panel-heading">Dashboard</div>
                    <div class="panel-body">
                        {{ trans('auth.failed') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</home>
@endsection
