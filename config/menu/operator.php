<?php
return [

    //Reservas
    'provider-menu.bookings' => [
        'icon' => 'fa fa-bed',
        'href' => '/aqui',
        'sub' =>  [
            'provider-menu.manage' => [
                'icon' => 'fa fa-table',
                'href' => 'bookings',
            ],
            'provider-menu.new_booking' => [
                'icon' => 'fa fa-table',
                'href' => 'booking/hotel/availables',
            ]
        ]
    ],

    //Usuarios
    'provider-menu.users' => [
        'icon' => 'fa fa-users',
        'href' => 'settings/companies/1#/membership',
        'sub' => []
    ],

];