<?php
return [
		
	//Reservas
	'provider-menu.bookings' => [
		'icon' => 'fa fa-bed',
		'href' => '/aqui',
		'sub' =>  [
			'provider-menu.manage' => [
				'icon' => 'fa fa-table',
				'href' => 'bookings',
			],
			'provider-menu.waiting_confirmation' => [
				'icon' => 'fa fa-hourglass-end',
				'href' => 'bookings-waiting-confirmation',
			],
			'provider-menu.history' => [
					'icon' => 'fa fa-history',
					'href' => 'provider-history',
			],
		]
	],
		
	//Acordos
	'provider-menu.according' => [
		'icon' => 'fa fa-suitcase',
		'href' => '/according',
		'sub' =>  [
			'provider-menu.operator' => [
				'icon' => 'fa fa-file',
				'href' => 'provider-a',
			],
			'provider-menu.brokers' => [
				'icon' => 'fa fa-file',
				'href' => 'brokerns-a',
			],
		]
	],

	//Tour
	'provider-menu.tour' => [
		'icon' => 'fa fa-camera',
		'href' => '',
		'sub' =>  [
			'provider-menu.tariff' => [
				'icon' => 'fa fa-file',
				'href' => 'aa'
			]
		]
	],
		
	//Usuarios
	'provider-menu.users' => [
		'icon' => 'fa fa-users',
		'href' => 'settings/companies/1#/membership',
		'sub' => []
	],
		
];