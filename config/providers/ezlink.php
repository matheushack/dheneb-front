<?php

$soap = [
    'wsdl' => [
        'hotel' => 'ws/2013/EZTour/Hotel.asmx?WSDL',
        'common' => 'ws/2013/common_a.asmx?WSDL',
        'client_backoffice' => 'ws/2013/ClientBackOffice_c.asmx?WSDL',
    ],
    'tags' => [
        'hotelResult' => 'HotelResultEZTour',
        'roomResult' => 'RoomResultEZTour',
        'hotelDetail' => 'HotelDetailWhithTripAdvisor_b',
        'roomResultCancellationPoliciesTag' => 'RoomResultCancellationPolicies',
        'hotelBookingRoomResult' => 'HotelBookingRoomResult',
    ],
];

$config =  [
    'local' => [
        'base_url' => 'https://homolog.cangooroo.net',
        'username' => 'dheneb',
        'password' => '@!987654321',
        'soap' => $soap
    ],

    'production' => [
        'base_url' => 'https://ws-ezlink.cangooroo.net',
        'username' => 'dheneb',
        'password' => '@!987654321',
        'soap' => $soap
    ],

];

return $config[env('APP_ENV', 'production')];