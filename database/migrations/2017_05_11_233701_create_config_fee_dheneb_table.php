<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfigFeeDhenebTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('config_fee_dheneb', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('line_bussine_id');
			$table->decimal('fee', 3);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('config_fee_dheneb');
	}

}
