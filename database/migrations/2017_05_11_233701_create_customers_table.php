<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 300);
			$table->string('surname', 300);
			$table->string('phone', 50);
			$table->string('email', 300);
			$table->string('document', 50);
			$table->string('address', 200)->nullable();
			$table->string('city', 200)->nullable();
			$table->char('country', 4)->nullable();
			$table->char('zipcode', 20)->nullable();
			$table->string('state', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers');
	}

}
