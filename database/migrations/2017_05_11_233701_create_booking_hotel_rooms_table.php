<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingHotelRoomsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking_hotel_rooms', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('booking_hotel_id');
			$table->integer('count')->default(1);
			$table->string('room', 40);
			$table->string('type', 20);
			$table->string('regime', 25);
			$table->integer('adults')->default(1);
			$table->integer('children')->default(0);
			$table->string('currency');
			$table->timestamps();
			$table->float('price', 53, 0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking_hotel_rooms');
	}

}
