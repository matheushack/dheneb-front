<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingToursTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking_tours', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->bigInteger('localizer');
			$table->string('localizer_prv', 200)->nullable();
			$table->integer('customer_id');
			$table->integer('company_id');
			$table->integer('provider_channel_id');
			$table->integer('user_id');
			$table->integer('tour_id');
			$table->decimal('base_amount', 18)->default(0);
			$table->decimal('additional_amount', 18)->default(0);
			$table->decimal('tot_amount', 18)->default(0);
			$table->char('currency', 4);
			$table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->dateTime('update_at')->nullable();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking_tours');
	}

}
