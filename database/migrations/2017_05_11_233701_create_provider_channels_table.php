<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProviderChannelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('provider_channels', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('provider_id');
			$table->integer('line_bussine_id');
			$table->string('provider_class');
			$table->boolean('enable')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('provider_channels');
	}

}
