<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingHotelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking_hotels', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id');
			$table->integer('hotel_id');
			$table->integer('booking_status_id');
			$table->integer('provider_id');
			$table->date('checkin')->nullable();
			$table->date('checkout')->nullable();
			$table->integer('adults')->nullable();
			$table->integer('children')->default(0);
			$table->string('currency')->nullable();
			$table->float('value', 53, 0)->nullable();
			$table->float('value_with_tax', 53, 0)->nullable();
			$table->date('deadline_change')->nullable();
			$table->date('deadline_cancellation')->nullable();
			$table->string('special_requests')->nullable();
			$table->string('sensitive_information')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking_hotels');
	}

}
