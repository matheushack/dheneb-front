<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingHotelRoomPassengersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking_hotel_room_passengers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('booking_hotel_room_id');
			$table->string('name', 50);
			$table->string('last_name', 50);
			$table->string('email')->nullable();
			$table->string('type');
			$table->integer('age')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking_hotel_room_passengers');
	}

}
