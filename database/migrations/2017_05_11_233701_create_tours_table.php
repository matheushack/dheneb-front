<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateToursTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tours', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('city_id');
			$table->integer('provider_channel_id');
			$table->string('code', 50);
			$table->string('title', 300);
			$table->string('description', 600);
			$table->char('duration', 5)->nullable();
			$table->integer('qty_avail')->default(0);
			$table->char('start_time', 5)->nullable();
			$table->char('end_time', 5)->nullable();
			$table->decimal('tot_amount', 18)->default(0);
			$table->char('currency', 5);
			$table->string('remark')->nullable();
			$table->decimal('deadline_daysbefore_cancellation', 2, 0)->nullable();
			$table->decimal('fee_cancellation', 18)->nullable();
			$table->string('itinerary');
			$table->string('days', 60);
			$table->char('private', 6)->nullable();
			$table->string('url_thumb', 300)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tours');
	}

}
