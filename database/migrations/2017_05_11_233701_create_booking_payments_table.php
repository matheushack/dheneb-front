<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking_payments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->bigInteger('booking_localizer');
			$table->char('card_flag', 2)->nullable();
			$table->string('card_number', 20)->nullable();
			$table->char('card_valid', 5)->nullable();
			$table->char('card_security_code', 5)->nullable();
			$table->string('card_holder', 200)->nullable();
			$table->string('card_doc_number', 100)->nullable();
			$table->decimal('total_paid', 18);
			$table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->dateTime('update_at')->nullable();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking_payments');
	}

}
