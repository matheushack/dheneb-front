<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfigPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('config_payments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('provider_id');
			$table->integer('payment_method_id');
			$table->decimal('installments', 2, 0)->nullable();
			$table->integer('lob_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('config_payments');
	}

}
