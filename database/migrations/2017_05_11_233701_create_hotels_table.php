<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHotelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hotels', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('city_id');
			$table->string('name');
			$table->string('address')->nullable();
			$table->string('phone1')->nullable();
			$table->string('phone2')->nullable();
			$table->text('description_en')->nullable();
			$table->text('description_es')->nullable();
			$table->text('description_pt')->nullable();
			$table->integer('stars')->default(1);
			$table->string('zip_code')->nullable();
			$table->string('url_thumb')->nullable();
			$table->string('web_site')->nullable();
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hotels');
	}

}
