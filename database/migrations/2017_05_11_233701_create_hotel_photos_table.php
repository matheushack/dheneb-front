<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHotelPhotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hotel_photos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('hotel_id');
			$table->string('url');
			$table->string('description_en');
			$table->string('description_es');
			$table->string('description_pt');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hotel_photos');
	}

}
