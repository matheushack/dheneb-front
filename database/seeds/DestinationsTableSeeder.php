<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class DestinationsTableSeeder extends Seeder
{

    public $destinationsArrayQueue = array();

    const QUEUE_LIMIT = 100;

    public function run()
    {
        
        $worldQuasarCities = function ($world){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://quasar.e-htl.com.br/destinations/cities?country={$world}");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            curl_close($ch);
            if (!empty($output)) {
                return json_decode($output, true)['data'];
            }
        };

        $worlds = require __DIR__ . "/files/array_world.php";

        $worlds = new Collection($worlds);

        foreach($worlds as $world) {
            $worlds_quasar = $worldQuasarCities($world["alpha3"]);
            foreach ($worlds_quasar as $world_quasar){
                DB::table('cities')->insert([
                    'quasar_id' => $world_quasar['id'],
                    'country_id' => $world_quasar["attributes"]['countryCode'],
                    'city_name_en' => $world_quasar["attributes"]['name'] . ' - ' . $world_quasar["attributes"]['district'],
                    'city_name_pt' => $world_quasar["attributes"]['name_pt'] . ' - ' . $world_quasar["attributes"]['district'],
                    'city_name_es' => $world_quasar["attributes"]['name_es'] . ' - ' . $world_quasar["attributes"]['district'],
                    'country' => $world_quasar["attributes"]['country'],
                    'type' => 'city',
                ]);
                echo $world_quasar["attributes"]['name'] .  ' - ' . $world_quasar["attributes"]['district'] . PHP_EOL;
            }
        }
        
    }


}
