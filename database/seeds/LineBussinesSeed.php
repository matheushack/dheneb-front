<?php

use Illuminate\Database\Seeder;

class LineBussinesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('line_bussines')->insert([
            'description' => 'Dheneb',
            'status' => '0',
        ]);

        DB::table('line_bussines')->insert([
            'description' => 'Hotel',
            'status' => '0',
        ]);

        DB::table('line_bussines')->insert([
            'description' => 'Aereo',
            'status' => '0',
        ]);

        DB::table('line_bussines')->insert([
            'description' => 'Seguro',
            'status' => '0',
        ]);

        DB::table('line_bussines')->insert([
            'description' => 'Tour',
            'status' => '0',
        ]);

        DB::table('line_bussines')->insert([
            'description' => 'Translado',
            'status' => '0',
        ]);

        DB::table('line_bussines')->insert([
            'description' => 'Ticket',
            'status' => '0',
        ]);

    }
}