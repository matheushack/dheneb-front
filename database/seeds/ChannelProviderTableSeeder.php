<?php

use Illuminate\Database\Seeder;

class ChannelProviderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('providers')->insert([
            'code' => 'EHTL ',
            'name' => 'E-HTL',
        ]);
        DB::table('providers')->insert([
            'code' => 'EZNK',
            'name' => 'EZLINK',
        ]);
        DB::table('providers')->insert([
            'code' => 'NAZCA ',
            'name' => 'NAZCATRAVEL',
        ]);

        Dheneb\Models\ProviderChannel::create([
        	'provider_id' => 1,
        	'line_bussine_id' => 2,
        	'provider_class' => 'Dheneb\Services\Channels\Providers\Hotel\Quasar\QuasarHotel',
        	'enable' => true
    	]);

        Dheneb\Models\ProviderChannel::create([
            'provider_id' => 2,
            'line_bussine_id' => 2,
        	'provider_class' => 'Dheneb\Services\Channels\Providers\Hotel\EzLink\EzlinkHotel',
        	'enable' => false
        ]);

        Dheneb\Models\ProviderChannel::create([
            'provider_id' => 3,
            'line_bussine_id' => 5,
            'provider_class' => '',
            'enable' => true
        ]);

    }
}
