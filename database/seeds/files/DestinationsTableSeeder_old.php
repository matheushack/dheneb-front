<?php

use Illuminate\Database\Seeder;

class DDDDDDestinationsTableSeeder extends Seeder
{

    public $destinationsArrayQueue = array();

    const QUEUE_LIMIT = 100;

    public function run()
    {
        $handler = $this->getFileHandler();

        $columnNames = $this->readColumnNames($handler);

        $this->importDestinations($handler, $columnNames);
    }

    public function getFileHandler()
    {
        return gzopen(__DIR__."/files/cangooroo_data_cache_destinos_2_20151228.csv.gz", "r");
    }

    public function readColumnNames($csvHandler)
    {
        return fgetcsv($csvHandler);
    }

    public function importDestinations($handler, $columnNames)
    {
        while($nextDestination = $this->getNextDestination($handler, $columnNames)) {
            $this->importDestination($nextDestination);
        }

        $this->processArrayQueue();
    }

    public function getNextDestination($csvHandler, $columnNames)
    {
        $data = fgetcsv($csvHandler);

        if(!$data) {
            return null;
        }

        return array_combine($columnNames, $data);
    }

    public function importDestination($nextDestination)
    {
        $type = $this->getType($nextDestination['DestinoTipo']);

        $destinationData = [
            'id' => $nextDestination['DestinoId'],
            'country_id' => $nextDestination['PaisId'],
            'city_name_en' => $nextDestination['NomeEN'],
            'city_name_pt' => $nextDestination['NomePT'],
            'city_name_es' => $nextDestination['NomeES'],
            'type' => $type,
        ];

        $this->pushToArrayQueue($destinationData);
    }

    public function getType($portugueseType)
    {
        $table = [
            'Cidade' => 'city',
            'Arredores' => 'surroundings',
            'Vizinhança' => 'neighborhood',
            'Ponto de interesse' => 'point_of_interest',
            'Aeroporto' => 'airport',
        ];

        if($type = array_get($table, $portugueseType)) {
            return $type;
        } else {
            throw new \InvalidArgumentException("Invalid type $portugueseType");
        }
    }

    public function pushToArrayQueue($destinationData)
    {
        $this->destinationsArrayQueue[] = $destinationData;
        $this->processArrayQueueIfNecessary();
    }

    public function processArrayQueueIfNecessary()
    {
        $isNecessary = count($this->destinationsArrayQueue) > self::QUEUE_LIMIT;

        if($isNecessary) {
            $this->processArrayQueue();
        }
    }

    public function processArrayQueue()
    {
        DB::table('cities')->insert($this->destinationsArrayQueue);

        $this->destinationsArrayQueue = array();
    }


}
