<?php

use Illuminate\Database\Seeder;
use Dheneb\Models\UserPerfil;

class UserProfileSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$userPerfil = new UserPerfil();
    	$userPerfil->perfil = 'Administrator';
    	$userPerfil->save();
    	
    	$userPerfil = new UserPerfil();
    	$userPerfil->perfil = 'Operator';
    	$userPerfil->save();
    	
    	
    	$userPerfil = new UserPerfil();
    	$userPerfil->perfil = 'Agency';
    	$userPerfil->save();
    	
    	$userPerfil = new UserPerfil();
    	$userPerfil->perfil = 'Provider';
    	$userPerfil->save();

    }
    
    
}
