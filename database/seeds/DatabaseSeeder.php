<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DestinationsTableSeeder::class);
        $this->call(UserProfileSeed::class);
        $this->call(BookingSeeder::class);
        $this->call(LineBussinesSeed::class);
        $this->call(ChannelProviderTableSeeder::class);
        $this->call(BookingHotelTableSeeder::class);
    }

}
