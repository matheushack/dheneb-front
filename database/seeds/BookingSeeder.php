<?php

use Illuminate\Database\Seeder;
use Dheneb\Models\BookingStatus;

class BookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BookingStatus::create([
            'status' => 'confirmed',
        ]);

        BookingStatus::create([
            'status' => 'canceled',
        ]);

        BookingStatus::create([
            'status' => 'waiting_provider',
        ]);

        BookingStatus::create([
            'status' => 'confirmed_waiting_payment',
        ]);
    }
}
